# Table des matières

[[_TOC_]]

# OneGeo-Maps

OneGeo-Maps est un composant qui permet de créer des applications
cartographiques et peut couvrir plusieurs usages :

-   C’est un webSIG qui peut être utilisé comme on utilise QGIS ou un SIG traditionnel pour consulter une donnée, croiser plusieurs données mais sans enregistrer son travail.
-   Il peut être utilisé comme outil de valorisation de données pour les mettre en contexte avec d'autres données
-   Il peut être utilisé pour créer des applications métiers, pour éditer des données de manière collaborative par exemple

Dans OneGeo-Maps, une application est :

-   Un ensemble de couches de données organisées, configurés, stylés
-   Un ensemble d'outils ou plugins activés et configurés
-   Une description de tout ça et une gestion des droits associées

Avec OneGeo-Maps, on va donc créer des applications et les mettre à
disposition soit via accès direct par une URL ou via la page d’accueil
qui permet de rechercher parmi les applications disponibles.

<img src="media/image1.png" width="800px" />

N’importe quel utilisateur authentifié pourra créer une application et
la partager avec les membres de son organisation ou la rendre accessible
publiquement.

# Fonctions

## Création d’une application :

Lorsqu’on créé une nouvelle application ou qu’on modifie la
configuration d’une application existante, des menus supplémentaires
apparaissent :

-   Le bouton « Réglages » <img src="media/image2.png"  /> permet de définir les paramètres généraux
-   Le bouton « Modules »<img src="media/image3.png"  /> permet de définir les fonctionnalités actives

En haut de la carte, un encart indique que la carte est en mode
« Edition » et propose les boutons pour annuler ou enregistrer les
modifications effectuées sur la configuration de l’application

<img src="media/image4.png" style="width:4.38889in;height:1.06944in" />

### Définir les Paramètres généraux :

Pour initier une application, on va commencer par lui donner un titre,
une description et ajouter une image de prévisualisation. Ces
informations seront affichées sur la page d’accueil qui liste les
applications. Pour l’image, on pourra utiliser le bouton « Faire une
copie d’écran pour prendre une capture de la carte courante.

On va définir aussi l’emprise par défaut qui sera celle utilisée à
l’ouverture de la carte. Et pour terminer on définira si besoin de
niveau d’accès à cette carte.

<img src="media/image5.png" width="400px" />


### Activer des fonctionnalités (Plugin) :

Quand on configure une application, on sélectionne les fonctionnalités
qui vont être active, on peut donc créer des applications simples au
design très épuré en désactivant pratiquement tous les outils ou des
applications plus complexes en activant plus de fonctionnalités

<img src="media/image6.png" width="400px" />

<table>
<colgroup>
<col style="width: 35%" />
<col style="width: 64%" />
</colgroup>
<thead>
<tr class="header">
<th>Fonctionnalités</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Navigation</td>
<td><p>Permet de rajouter les boutons Zoom +, Zoom- et Vue Globale</p>
<p><img src="media/image7.png"
style="width:1.21568in;height:0.4899in" /></p></td>
</tr>
<tr class="even">
<td>Échelle</td>
<td><p>Permet d’ajouter l’échelle graphique à la carte</p>
<p><img src="media/image8.png"
style="width:1.32073in;height:0.44353in" /></p></td>
</tr>
<tr class="odd">
<td>Liste d’Échelles</td>
<td><p>Permet d’ajouter la fonction de zoom sur une échelle
prédéfinie</p>
<p><img src="media/image9.png" style="width:0.95892in;height:2.13913in"
alt="Une image contenant texte Description générée automatiquement" /></p></td>
</tr>
<tr class="even">
<td>Coordonnées</td>
<td><p>Permet de configurer et d’activer l’affichage des coordonnées de
la souris sur la carte</p>
<p><img src="media/image10.png" width="400px" /></p>
<p><img src="media/image11.png" width="400px" /></p></td>
</tr>
<tr class="odd">
<td>Recherche de lieux</td>
<td><p>Active la fonction de localisation par adresse</p>
<p><img src="media/image12.png"
width="400px" /></p></td>
</tr>
<tr class="even">
<td>Indicateur de chargement</td>
<td><p>Active l’affichage de l’indicateur de chargement des tuiles qui
permet de savoir si la carte est entièrement chargée</p>
<p><img src="media/image13.png" width="400px" /></p></td>
</tr>
<tr class="odd">
<td>Légende</td>
<td><p>Active l’affichage de la légende</p>
<p><img src="media/image14.png" width="400px" /></p></td>
</tr>
<tr class="even">
<td>Impression</td>
<td><p>Active la fonction d’impression</p>
<p><img src="media/image15.png"
width="400px" /></p></td>
</tr>
<tr class="odd">
<td>Fiche Information</td>
<td><p>Active l’affichage d’une fiche information quand on clique sur un
objet</p>
<p><img src="media/image16.png" width="400px" /></p></td>
</tr>
<tr class="even">
<td>Mesure de distance/surfaces</td>
<td><p>Active l’affichage des outils de mesure</p>
<p><img src="media/image17.png" width="400px" /></p></td>
</tr>
<tr class="odd">
<td>Permaliens</td>
<td><p>Active l’affichage du panneau permalien</p>
<p><img src="media/image18.png" width="400px" /></p></td>
</tr>
</tbody>
</table>

### Ajouter des données :

Quand vous créez une application, vous pouvez rajouter des données
diffusées à partir de services OGC (WMS, WFS et WMTS). Il faut ensuite
les organiser (il est possible de créer des groupes, de les réordonner)
et les configurer.

<img src="media/image19.png" width="800px" />

Suivant le type de couche (WMS, WFS, WMTS, Vecteur Tuilé), les
possibilités de paramétrage vont varier mais on va pouvoir configurer :

-   Le Titre qui apparait dans la liste des couches

-   La Description qui apparait lorsque la souris survole le bouton
    <img src="media/image20.png" style="width:0.16528in;height:0.16528in" />

-   Le lien vers une page web décrivant les métadonnées de la couche

-   Les informations d’authentification si nécessaire

-   Le type de géométrie pour les couches vecteur éditables

-   Un filtre qui permettra de restreindre les objets affichés selon des
    critères attributaires (par exemple code\_insee=’36000’ ou
    population &gt; 25000 )

-   Les échelles min et max de visibilité de la couche

-   L’affichage ou non de la légende pour cette couche

-   L’emprise de la couche notamment utilisé pour le bouton « Zoomer sur
    l’étendue de la couche »
    <img src="media/image21.png" style="width:1.58333in;height:0.68056in"
    alt="Une image contenant texte Description générée automatiquement" />

-   Un Copyright qui sera affiché sur la carte et les impressions si la
    couche est affichée

<img src="media/image22.png" width="400px"
alt="Une image contenant texte Description générée automatiquement" />

<img src="media/image23.png" width="400px"
alt="Une image contenant texte Description générée automatiquement" />

Dans l’onglet « Attributs », l’utilisateur peut :

-   cocher les attributs qui doivent apparaitre dans la fiche d’information
-   donner un libellé qui remplacera le nom de l’attribut dans la fiche d’information
-   cocher les attributs qui doivent apparaitre dans le formulaire d’édition si la couche est éditable

Dans le cas d’une couche d’annotations, l’utilisateur pourra aussi créer
des attributs et en définir le type.

<img src="media/image24.png" width="400px"
alt="Une image contenant table Description générée automatiquement" />

Dans l’onglet « Style », les possibilités vont varier en fonction du
type de couche (WMS, WFS ou Vecteur Tuilé ) et du type de géométrie
définie.

Le style pourra être exporté en SLD ce qui permettra de l’importer dans
Geoserver ou QGIS.

<img src="media/image25.png" width="400px" />

## Autres Fonctionnalités

### Edition

Une couche éditable possède un bouton qui permet de passer en mode
« Edition » pour cette couche.

<img src="media/image26.png" width="400px" />

Lorsqu’on passe en mode édition, la barre d’édition s’affiche et permet
de créer/modifier/supprimer des objets et modifier les attributs qui ont
été déclarés éditables dans la configuration des attributs.

<img src="media/image27.png" width="400px" />

Certains outils sont masqués si on a défini le type de géométrie de la
couche (par exemple dessin de point, polygone et cercle si on a défini
qu’il s’agit d’une couche de lignes)

<img src="media/image28.png" width="400px" />

### Affichage de la table attributaire et filtrage

Pour les données WFS, il est possible d’afficher la liste des objets
sous forme de tableur et de rajouter des filtres spatiaux ou
attributaires pour filtrer les résultats affichés.

<img src="media/image29.png" width="400px" />
