[[_TOC_]]

# Mise en place de l'environnement de developpement

## Prérequis 

* Node / npm
* un serveur web (apache ou nginx)


### configuration apache pour utiliser le backend

1- Activer les modules apaches suivants (dans le httpd.conf, exemple sous mac):
```
LoadModule proxy_module libexec/apache2/mod_proxy.so
LoadModule proxy_http_module libexec/apache2/mod_proxy_http.so
LoadModule rewrite_module libexec/apache2/mod_rewrite.so
LoadModule ssl_module libexec/apache2/mod_ssl.so
LoadModule headers_module libexec/apache2/mod_headers.so
```

2- Ajouter la configuration suivante dans un ficher maps.conf par exemple dans /etc/apache2/other/maps.conf
```
SSLProxyEngine On
ProxyPass /onegeo http://localhost:8080
ProxyPass /onegeo-demo https://demo.onegeo.fr
ProxyPass /pigmadev https://preprod.pigma.org
ProxyPass /pigmaprod https://www.pigma.org
```

## Récupération des sources et lancement
```
git clone https://git.neogeo.fr/onegeo-suite/sites/onegeo-suite-site-maps-vuejs.git
cd onegeo-suite-site-maps-vuejs
npm install
npm run serve
```

le fichier de configuration principal se trouve dans public/sample_data/config.json, il est possible d'adapter les urls pour pointer vers le bon backend (pigma / démo )

Ensuite ouvrir http://localhost/onegeo/#/map/new pour voir si la création de cartes fonctionne

### problèmes 

si la version de node est récente et qu'il y a l'erreur suivante ERR_OSSL_EVP_UNSUPPORTED 

```
export NODE_OPTIONS=--openssl-legacy-provider
npm run serve
```


# Configuration Générale 

le fichier de configuration config.json est chargé au démarrage et permet de paramétrer la configuration globale de l'application

* base_api_url : 
* maps_api_url : base url des webservice de gestion des contextes de carte
* catalog_url : base url du service elasticSearch pour la recherche de données dans le catalogue interne
* documentation_url : lien vers la documentation
* cors_proxy_url: url du proxy pour bypasser les problèmes CORS
* addLayers : objet permettant d'activer/desactiver les différents onglets d'ajout de couches
```
    "ogc":true, // affiche l'onglet pour ajouter des couches OGC
    "geojson":true, // affiche l'onglet pour ajouter des couches Geojson
    "catalog":true, // affiche l'onglet pour ajouter des couches depuis le catalogue interne
    "osm":true // affiche l'onglet pour ajouter des fond de plan
```




# Plugins


les plugins sont des fonctionnalités qui sont activables/désactivables pour chaque application. <br>
Il y a des plugins qui s'intègrent dans la carte (au dessus de la carte) et d'autres qui rajoutent des menus dans la barre de gauche. <br>
La zone de carte est divisée en 4 espaces (bas-gauche,haut-gauche,bas-droit,haut-droit) dans lequel les interfaces des plugins vont s'ajouter quand ils sont intégrés à une application. Chaque plugin est à une localisation parmis ces 5 possibilitées (4 zones sur la carte + menu de gauche)

## Liste des emplacements pour les plugins
```
export enum PluginLocation
{
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight,
    LeftToolBar,
}
```

## Paramètrage des plugins

Certains plugins sont paramétrables, c'est à dire que l'utilisateur va pouvoir activer le plugin et jouer sur un certains nombre de paramètres pour définir son comportement.
Par exemple, on pourra préciser le système de coordonnées pour l'affichage des coordonnées de la souris, ou le nombre de chiffre significatifs à afficher.

Les paramètres peuvent avoir les types suivants :
* Boolean : true ou false éditable avec une case à cocher 
* Select : texte parmis une liste de valeurs possible éditable avec une combo
* Text : texte libre
* Number : nombre

## Liste des plugins :

| Id    | label | Description | Paramétrage | 
| ------ | ------   | ------ | ------ |
| navigation | Navigation | Boutons de zoom + et - et Bouton pour revenir à la vue intiale | * zoomInOut (Boolean):true pour afficher les boutons zoom + et - <br> * zoomToMaxExtent (Boolean):true pour afficher le bouton de retour à la vue initiale  |
| scale | Echelle | Affichage d'une echelle graphique sur la carte |   |
| scale-list | Liste d'Echelles | Affichage d'une liste d'échelle prédéfinie |   |
| coordonnes | Coordonnées | Affichage des coordonnées de la souris |  * displayProj (liste): choix de la projection(2154,4326,3857) <br> * format (texte):texte à afficher par exemple<br> X:{x} Y:{y} (Lambert 93) <br> * precision (entier): Nombre de chiffres significatifs  |
| recherche-lieux | Recherche de lieux | Rechercher et localiser des adresses et lieux-dits avec l'api adresse (https://api-adresse.data.gouv.fr/search/?q=) | * result-count (entier): nombre de résultats à afficher  |
| loading-infos | Indicateur de chargement | Affiche une indication lorsque la carte n'est pas complètement chargée. |   |
| legend | Légende | Affiche un menu qui active un panneau de légende |   |
| print | Impression | Affiche un menu qui active un panneau d'Impression |   |
| feature-infos | Fiche Information | Affiche une fiche info lorsqu\'on clique sur un objet de la carte |   |
| measure | Mesure de distances/surfaces | Afficher les boutons de mesures de distances/surfaces. |   |
| permalink | Permaliens | Affiche un permalien pour partager la carte |   |




# Organisation du code



## Modèle de données

TODO

## Services

TODO

```mermaid
graph TD;
  AbstractMapService-->CesiumMapService;
  AbstractMapService-->MapService;
  AbstractSelectionService-->CesiumSelectionService;
  AbstractSelectionService-->SelectionService;
  PluginService
  CapabilitiesService
  ContextService
  EditionService

```
