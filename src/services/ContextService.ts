/* eslint-disable import/no-cycle */
/* eslint-disable class-methods-use-this */
import AbstractLayerTreeElement from '@/models/map-context/AbstractLayerTreeElement';
import AuthenticationInfos from '@/models/map-context/AuthenticationInfos';
import { pluginServiceInstance, PluginService } from '@/services/PluginService';
import MapContext from '@/models/map-context/MapContext';
import Plugin from '@/models/plugin/Plugin';
import axios, { AxiosResponse } from 'axios';
import Application from '@/models/map-context/Application';
import { BIconFileEarmarkPerson } from 'bootstrap-vue';
import LayerConfig from '@/models/map-context/LayerConfig';
import ColorPalette, { IColorPalette } from '@/models/ColorPalette';
import { Observable } from 'rxjs';
import { EventBus, EVENTS } from './EventBus';

class ContextService {
  currentContext: MapContext |null= null;

  currentApplication: Application |null= null;

  currentActionTarget!: any;

  currentAction='';

  termsValidationRequired = false;

  isEditionActive = false;

  contentEditionIsAllowed = true;

  configLoaded = false;

  tablelayer:LayerConfig|null = null; // couche actuelle affichée dans le composant FeatureListPanel

  loading = false; // gère l'affichage de la fenetre de chargement

  user:any = null;
  // user:any = { email: 'dds', is_superuser: true, usergroup_roles: [{ usergroup: { id: 1 } }] };//= null;

  private draggingLayer: AbstractLayerTreeElement | null=null;

  configuration!:any;

  svgPictos!:any;

  colorPalettes:ColorPalette[]=[];

  private localStorageAuthDB:any = {};

  constructor() {
    const localInfos = localStorage.getItem('localStorageAuthDB');

    if (localInfos !== undefined && localInfos !== null) {
      this.localStorageAuthDB = JSON.parse(localInfos);
    }

    this.loadConfig().then((result) => {
      console.log('config loaded');
      this.configLoaded = true;
      this.configuration = result.data;
      EventBus.$emit(EVENTS.CONFIG_LOADED, this.configuration);
      this.loadUserStatus();
    });

    this.loadSvgPictos().then((result) => {
      console.log('pictos loaded');
      this.svgPictos = result.data;
    });
    this.loadPaletteList().then((result) => {
      console.log('palettes loaded');
      this.colorPalettes = result.data.palettes.map((x:IColorPalette) => new ColorPalette(x));
    });
  }

  createMap():void {
    // delete old map
    this.currentContext = null;
    this.getApp('empty_template.json').then((reponse) => {
      console.log(reponse.data);
      const retour = reponse.data;
      const newApp = new Application({
        display_name: 'Mon appli',
      });
      const currentAppConfig = new MapContext(retour);
      this.loadContext(newApp, currentAppConfig);
      EventBus.$emit(EVENTS.OPEN_LEFT_SIDE_MENU, 'SettingsPanel');
    });
  }

  getDraggingLayer(): AbstractLayerTreeElement | null{
    return this.draggingLayer;
  }

  setDraggingLayer(layer:AbstractLayerTreeElement | null) :void {
    console.log('set dragging layer', layer);
    this.draggingLayer = layer;
  }

  loadContext(application:Application, appConfig: MapContext): void {
    this.currentApplication = application;
    this.currentContext = appConfig;
  }

  startEdition() :void{
    this.isEditionActive = true;
  }

  cancelEdition() :void{
    this.isEditionActive = false;
  }

  getCurrentApplication(): Application {
    if (this.currentApplication == null) {
      throw new Error('Application not loaded!');
    }
    return this.currentApplication;
  }

  getCurrentContext(): MapContext {
    if (this.currentContext == null) {
      throw new Error('Context not loaded!');
    }
    return this.currentContext;
  }

  isContextLoaded(): boolean {
    return this.currentContext !== null;
  }

  setCurrentAction(action: string, target: any): void {
    this.currentActionTarget = target;
    this.currentAction = action;
  }

  getCsrfToken():string {
    const csrfToken:any = ((name) => {
      const re = new RegExp(`${name}=([^;]+)`);
      const value = re.exec(document.cookie);
      return (value != null) ? unescape(value[1]) : null;
    })('csrftoken');
    return csrfToken;
  }

  removeApp(map: Application):Promise<any> {
    return axios.delete(`${this.getMapsApiUrl()}/${map.id}/`, { headers: { 'X-CSRFToken': this.getCsrfToken() } }).then((result) => {
      console.log(result);
    });
  }

  deleteCurrentMap():Promise<any> {
    if (this.currentApplication !== null) {
      return this.removeApp(this.currentApplication);
    }
    throw new Error('Aucune application');
  }

  getJsonContext():any {
    if (this.currentContext !== null) {
      const mapContext = this.currentContext.toJson();
      pluginServiceInstance.plugins
        .forEach((plugin:Plugin) => mapContext.plugins.push(plugin.toJson()));
      return mapContext;
    }
    throw new Error('currentContext is null');
  }

  saveContext():Promise<any> {
    const promise = new Promise((resolve, reject) => {
      if (this.currentContext !== null) {
        const mapContext = this.getJsonContext();
        console.log(JSON.stringify(mapContext));

        if (this.currentApplication !== null) {
          this.currentApplication.config = mapContext;// JSON.stringify(mapContext);
          // eslint-disable-next-line @typescript-eslint/no-this-alias
          const self = this;
          const csrfToken = this.getCsrfToken();

          let request;

          if (this.currentApplication.id !== undefined) {
            request = axios.put(`${this.getMapsApiUrl()}/${this.currentApplication.id}/`,
              this.currentApplication,
              { headers: { 'X-CSRFToken': csrfToken } });
          } else {
            request = axios.post(`${this.getMapsApiUrl()}/`,
              this.currentApplication,
              { headers: { 'X-CSRFToken': csrfToken } });
          }
          this.loading = true;
          EventBus.$emit(EVENTS.INFO, 'Sauvegarde en cours....');
          self.isEditionActive = false;
          request.then((res) => {
            console.log(res);
            self.loading = false;
            EventBus.$emit(EVENTS.INFO, 'Sauvegarde effectuée');
            // eslint-disable-next-line prefer-destructuring
            const id = res.data.id;
            self.getCurrentApplication().id = id;
            if (self.currentApplication?.thumbnail != null) {
              const file:any = self.currentApplication?.thumbnail;
              const formData = new FormData();
              formData.append('file', file);
              const config = {
                headers: {
                  'Content-Type': 'multipart/form-data',
                  'X-CSRFToken': csrfToken,
                },
              };
              axios.put(`${this.getMapsApiUrl()}/${id}/thumbnail/`, formData, config).then((response) => {
                console.log(response);
                resolve(self.getCurrentApplication());
              });
            } else {
              resolve(self.getCurrentApplication());
            }
          }, (res) => {
            console.log(res);
            self.loading = false;
            self.isEditionActive = true;
            if (res.response.data && res.response.data.error) {
              EventBus.$emit(EVENTS.ERROR, res.response.data.error);
            } else {
              EventBus.$emit(EVENTS.ERROR, res);
            }
          });
        }
      }
    });
    return promise;
  }

  loadConfig():Promise<AxiosResponse<any>> {
    const url = './sample_data/config.json';
    return axios.get(url);
  }

  loadSvgPictos():Promise<AxiosResponse<any>> {
    return axios.get('./styles/svg_icons.json');
  }

  loadUserStatus(): void {
    const url = `${this.getBaseApiUrl()}login/session/`;
    axios.get(url).then((result) => {
      if (result.data.authenticated) {
        this.user = result.data.user;
      } else {
        this.user = null;
      }
    }).catch((error) => {
      console.log(error.response);
      if (error.response.status === 403 && error.response.data.code === 'terms-required') {
        this.termsValidationRequired = true;
      }
    });
  }

  logout() :void{
    const url = `${this.getBaseApiUrl()}login/signout/`;
    axios.get(url).then((result) => {
      this.user = null;
    });
  }

  isDatavizEnabled():boolean {
    return this.configuration.dataviz && this.configuration.dataviz.active;
  }

  isComplexStyleEnabled():boolean {
    return this.configuration.complexStyling && this.configuration.complexStyling.active;
  }

  getBaseApiUrl():string {
    return this.configuration.base_api_url;
  }

  getCorsProxyUrl():string {
    return this.configuration.cors_proxy_url;
  }

  getMapsApiUrl():string {
    return this.configuration.maps_api_url;
  }

  getMapById(id:number):Promise<AxiosResponse<any>> {
    return axios.get(`${this.getMapsApiUrl()}/${id}/`);
  }

  getFavoriteMaps(pageNumber = 1, pageSize = 10):Promise<AxiosResponse<any>> {
    return axios.get(`${this.getMapsApiUrl()}/?format=json&highlight=true&page=${pageNumber}&page_size=${pageSize}`);
  }

  getMaps(query = '', pageNumber = 1, pageSize = 3):Promise<AxiosResponse<any>> {
    return axios.get(`${this.getMapsApiUrl()}/?format=json&q=${query}&highlight=true&page=${pageNumber}&page_size=${pageSize}`);
  }

  getThumbnailUrl(map: Application): string {
    if (map.thumbnail != null) {
      return `${this.getMapsApiUrl()}/${map.id}/thumbnail/`;
    }
    throw new Error('thumbnail is null');
  }

  getProxyFiedUrl(url: string):string {
    // return `${this.getBaseApiUrl()}/proxy?url=${encodeURIComponent(url)}`;
    return `${this.getCorsProxyUrl()}${url}`;
  }

  loadOGCServerList():Promise<AxiosResponse<any>> {
    const url = './sample_data/serverList.json';
    return axios.get(url);
  }

  loadPaletteList():Promise<AxiosResponse<any>> {
    const url = './sample_data/color_palettes.json';
    return axios.get(url);
  }

  getApp(appId: number | string) :Promise<AxiosResponse<any>> {
    const url = `./sample_data/${appId}`;
    return axios.get(url);
  }

  loadUrl(url: string) :Promise<AxiosResponse<any>> {
    return axios.get(url);
  }

  removeAuthInfo(key: string) :void {
    delete this.localStorageAuthDB[key];
    localStorage.setItem('localStorageAuthDB', JSON.stringify(this.localStorageAuthDB));
  }

  addAuthInfo(key: string, authenticationInfos: AuthenticationInfos) :void {
    this.localStorageAuthDB[key] = authenticationInfos;
    localStorage.setItem('localStorageAuthDB', JSON.stringify(this.localStorageAuthDB));
  }

  getAuthInfos() :any {
    return this.localStorageAuthDB;
  }
}
const contexteServiceInstance = new ContextService();
export default ContextService;
export { ContextService, contexteServiceInstance };
