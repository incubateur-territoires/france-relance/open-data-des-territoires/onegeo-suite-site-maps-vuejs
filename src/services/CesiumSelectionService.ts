/* eslint-disable no-continue */
/* eslint-disable no-underscore-dangle */
/* eslint-disable import/no-cycle */
/* eslint-disable consistent-return */
/* eslint-disable no-param-reassign */
/* eslint-disable @typescript-eslint/no-this-alias */
/* eslint-disable import/no-named-as-default */
/* eslint-disable class-methods-use-this */
import LayerConfig from '@/models/map-context/LayerConfig';
import WMSGetFeatureInfo from 'ol/format/WMSGetFeatureInfo';
import AbstractSelectionService from './AbstractSelectionService';
import CesiumMapService from './CesiumMapService';
import EventBus, { EVENTS } from './EventBus';

declare let Cesium: any;

export default class CesiumSelectionService extends AbstractSelectionService {
  mapService!: CesiumMapService;

  tileselection: any = {};

  timerId!:any;

  content!:any;

  viewer:any;

  constructor(mapService:CesiumMapService) {
    super();
    this.mapService = mapService;
    this.viewer = this.mapService.viewer;

    this.viewer.screenSpaceEventHandler.setInputAction(
      this.pickAndSelectObject.bind(this),
      Cesium.ScreenSpaceEventType.LEFT_CLICK,
    );
  }

  pickAndSelectObject(e:any):void {
    this.selectFeaturesAtPixel(e);
    // this.viewer.selectedEntity = this.pickEntity(e);
  }

  pickEntity(e:any):any {
    const { viewer } = this;
    const picked = viewer.scene.pick(e.position);
    if (Cesium.defined(picked)) {
      const id = Cesium.defaultValue(picked.id, picked.primitive.id);
      if (id instanceof Cesium.Entity) {
        return id;
      }
      /*
      if (picked instanceof Cesium.Cesium3DTileFeature) {
        return new Cesium.Entity({
          name: getCesium3DTileFeatureName(picked),
          description: getCesium3DTileFeatureDescription(picked),
          feature: picked,
        });
      }
      */
    }

    const ray = viewer.camera.getPickRay(e.position);

    // No regular entity picked.  Try picking features from imagery layers.
    if (Cesium.defined(viewer.scene.globe)) {
      return this.pickImageryLayerFeatureTEST(viewer, e.position);
    }
  }

  pickImageryLayerFeatureTEST(viewer:any, windowPosition:any) {
    console.log('pickImageryLayerFeatureTEST');
    const { scene } = viewer;
    const pickRay = scene.camera.getPickRay(windowPosition);
    // Find the picked location on the globe.
    const pickedPosition = scene.globe.pick(pickRay, scene);
    if (!Cesium.defined(pickedPosition)) {
      return;
    }
    const pickedLocation = scene.globe.ellipsoid.cartesianToCartographic(pickedPosition);

    const imagery = this.pickImageryHelper(scene, pickedLocation, true);

    const provider = imagery.imageryLayer.imageryProvider;
    if (Cesium.defined(provider._tileProvider)) {
      const ressource = this.buildPickFeaturesResource(provider._tileProvider, imagery.x,
        imagery.y,
        imagery.level,
        pickedLocation.longitude,
        pickedLocation.latitude, 'application/json');

      console.log(ressource);
    }
  }

  buildPickFeaturesResource(
    imageryProvider:any,
    x:any,
    y:any,
    level:any,
    longitude:any,
    latitude:any,
    format:any,
  ) {
    const degreesScratchComputed = false;
    const projectedScratchComputed = false;
    const ijScratchComputed = false;
    const longitudeLatitudeProjectedScratchComputed = false;
    const templateRegex = /{[^}]+}/g;

    const resource = imageryProvider._pickFeaturesResource;
    const url = resource.getUrlComponent(true);
    const allTags = imageryProvider._pickFeaturesTags;
    const templateValues:any = {};
    const match = url.match(templateRegex);
    if (Cesium.defined(match)) {
      match.forEach((tag:any) => {
        const key = tag.substring(1, tag.length - 1); // strip {}
        if (Cesium.defined(allTags[key])) {
          templateValues[key] = allTags[key](
            imageryProvider,
            x,
            y,
            level,
            longitude,
            latitude,
            format,
          );
        }
      });
    }

    return templateValues;
  }

  pickImageryHelper(scene:any, pickedLocation:any, pickFeatures:any) {
    // Find the terrain tile containing the picked location.
    const tilesToRender = scene.globe._surface._tilesToRender;
    let pickedTile;
    const applicableRectangleScratch = new Cesium.Rectangle();
    for (
      let textureIndex = 0;
      !Cesium.defined(pickedTile) && textureIndex < tilesToRender.length;
      textureIndex += 1
    ) {
      const tile = tilesToRender[textureIndex];
      if (Cesium.Rectangle.contains(tile.rectangle, pickedLocation)) {
        pickedTile = tile;
      }
    }

    if (!Cesium.defined(pickedTile)) {
      return;
    }

    // Pick against all attached imagery tiles containing the pickedLocation.
    const imageryTiles = pickedTile.data.imagery;

    for (let i = imageryTiles.length - 1; i >= 0; i -= 1) {
      const terrainImagery = imageryTiles[i];
      const imagery = terrainImagery.readyImagery;
      if (!Cesium.defined(imagery)) {
        continue;
      }
      const provider = imagery.imageryLayer.imageryProvider;
      if (pickFeatures && !Cesium.defined(provider.pickFeatures)) {
        continue;
      }

      if (!Cesium.Rectangle.contains(imagery.rectangle, pickedLocation)) {
        continue;
      }

      // If this imagery came from a parent, it may not be applicable to its entire rectangle.
      // Check the textureCoordinateRectangle.
      const applicableRectangle = applicableRectangleScratch;

      const epsilon = 1 / 1024; // 1/4 of a pixel in a typical 256x256 tile.
      applicableRectangle.west = Cesium.Math.lerp(
        pickedTile.rectangle.west,
        pickedTile.rectangle.east,
        terrainImagery.textureCoordinateRectangle.x - epsilon,
      );
      applicableRectangle.east = Cesium.Math.lerp(
        pickedTile.rectangle.west,
        pickedTile.rectangle.east,
        terrainImagery.textureCoordinateRectangle.z + epsilon,
      );
      applicableRectangle.south = Cesium.Math.lerp(
        pickedTile.rectangle.south,
        pickedTile.rectangle.north,
        terrainImagery.textureCoordinateRectangle.y - epsilon,
      );
      applicableRectangle.north = Cesium.Math.lerp(
        pickedTile.rectangle.south,
        pickedTile.rectangle.north,
        terrainImagery.textureCoordinateRectangle.w + epsilon,
      );
      if (!Cesium.Rectangle.contains(applicableRectangle, pickedLocation)) {
        continue;
      }

      return imagery;
    }
  }

  pickImageryLayerFeature(viewer:any, windowPosition:any) {
    const { scene } = viewer;
    const pickRay = scene.camera.getPickRay(windowPosition);
    const imageryLayerFeaturePromise = scene.imageryLayers.pickImageryLayerFeatures(
      pickRay,
      scene,
    );

    if (!Cesium.defined(imageryLayerFeaturePromise)) {
      return;
    }

    // Imagery layer feature picking is asynchronous, so put up a message while loading.
    const loadingMessage = new Cesium.Entity({
      id: 'Loading...',
      description: 'Loading feature information...',
    });
    const self = this;
    imageryLayerFeaturePromise.then((features:any) => {
      if (viewer.selectedEntity !== loadingMessage) {
        return;
      }

      if (!Cesium.defined(features) || features.length === 0) {
        viewer.selectedEntity = self.createNoFeaturesEntity();
        return;
      }

      // Select the first feature.
      const feature = features[0];
      let feats = (new WMSGetFeatureInfo())
        .readFeatures(feature.data.ownerDocument.documentElement.outerHTML);
      console.log(feats);
      feats = feats.map((f:any) => { f.layerConfig = feature.imageryLayer.customConfig; return f; });
      EventBus.$emit(EVENTS.SELECT_FEATURE, feats);
      Cesium.GeoJsonDataSource.load(feats).then(
        (dataSource:any) => {
          const p = dataSource.entities.values;
          for (let i = 0; i < p.length; i += 1) {
            p[i].polygon.extrudedHeight = 15; // or height property
          }
          viewer.dataSources.add(dataSource);
          viewer.zoomTo(dataSource);
        },
      );
      /*
      const entity = new Cesium.Entity({
        id: feature.name,
        description: feature.description,
      });
      const cartesian3Scratch = new Cesium.Cartesian3();
      if (Cesium.defined(feature.position)) {
        const ecfPosition = viewer.scene.globe.ellipsoid.cartographicToCartesian(
          feature.position,
          cartesian3Scratch,
        );
        entity.position = new Cesium.ConstantPositionProperty(ecfPosition);
      }
      */
      // viewer.selectedEntity = entity;
    });

    return loadingMessage;
  }

  createNoFeaturesEntity():any {
    return new Cesium.Entity({
      id: 'None',
      description: 'No features found.',
    });
  }

  cleanSelection() {
    // throw new Error('Method not implemented.');
  }

  cleanInfoBulle() {
    // throw new Error('Method not implemented.');
  }

  getFeatureInfoUrlCesium(event: any, wmsOptions: any, layer: LayerConfig):any {
    const { scene } = this.viewer;
    const pickRay = scene.camera.getPickRay(event.position);
    // Find the picked location on the globe.
    const pickedPosition = scene.globe.pick(pickRay, scene);
    if (!Cesium.defined(pickedPosition)) {
      return;
    }
    // const pickedLocation = scene.globe.ellipsoid.cartesianToCartographic(pickedPosition);
    const pickedLocation = Cesium.Ellipsoid.WGS84.cartesianToCartographic(pickedPosition);

    const imagery = this.pickImageryHelper(scene, pickedLocation, true);

    const provider = imagery.imageryLayer.imageryProvider;
    if (Cesium.defined(provider._tileProvider)) {
      const ressource = this.buildPickFeaturesResource(provider._tileProvider, imagery.x,
        imagery.y,
        imagery.level,
        pickedLocation.longitude,
        pickedLocation.latitude, 'application/json');

      console.log(ressource);
      let url = layer.getFeatureInfoUrl();
      if (url === '') {
        return undefined;
      }
      url += `&I=${ressource.i}&J=${ressource.i}&WIDTH=${ressource.width}&HEIGHT=${ressource.height}&STYLES=&CRS=CRS:84&BBOX=${ressource.westProjected},${ressource.southProjected},${ressource.eastProjected},${ressource.northProjected}`;
      console.log(url);
      Cesium.GeoJsonDataSource.load(url).then(
        (dataSource:any) => {
          console.log(dataSource);
          const p = dataSource.entities.values;
          const toremove = [];
          for (let i = 0; i < p.length; i += 1) {
            if (i === 0) {
              p[i].polygon.material = Cesium.Color.WHITE;
              p[i].polygon.extrudedHeight = 1500;
            } else {
              toremove.push(p[i]);
            }
          }
          toremove.forEach((pe:any) => dataSource.entities.remove(pe));
          this.viewer.dataSources.removeAll();
          this.viewer.dataSources.add(dataSource);
          dataSource.show = true;
          // this.viewer.zoomTo(dataSource);
        },
      );
      return url;
    }
    return '';
  }

  getFeatureInfoUrl(event: any, wmsOptions: any, layer: LayerConfig) {
    const ray = this.viewer.camera.getPickRay(event.position);
    const cartesian = this.viewer.scene.globe.pick(ray, this.viewer.scene);

    const cartographic = Cesium.Ellipsoid.WGS84.cartesianToCartographic(cartesian);
    console.log(cartographic);
    const lon = Cesium.Math.toDegrees(cartographic.longitude);
    const lat = Cesium.Math.toDegrees(cartographic.latitude);
    const delta = 0.02;
    const bbox = `${lon - delta},${lat - delta},${lon + delta},${lat + delta}`;

    let url = layer.getFeatureInfoUrl();
    // const viewResolution = /** @type {number} */ (this.mapService.view.getResolution());
    url += `&I=50&J=50&WIDTH=100&HEIGHT=100&STYLES=&CRS=CRS:84&BBOX=${bbox}`;
    console.log(url);
    Cesium.GeoJsonDataSource.load(url).then(
      (dataSource:any) => {
        console.log(dataSource);
        const p = dataSource.entities.values;
        const toremove = [];
        for (let i = 0; i < p.length; i += 1) {
          if (i === 0) {
            p[i].polygon.material = Cesium.Color.WHITE;
            p[i].polygon.extrudedHeight = 1500;
          } else {
            toremove.push(p[i]);
          }
        }
        toremove.forEach((pe:any) => dataSource.entities.remove(pe));
        this.viewer.dataSources.removeAll();
        this.viewer.dataSources.add(dataSource);
        dataSource.show = true;
      },
    );
    return url;
  }

  selectFeatures(allFeatures: any, infobulle: boolean, event: any) {
    if (!allFeatures || allFeatures.length === 0 || !allFeatures[0].layerConfig) {
      console.error('could not find layer for this feature');
      return;
    }

    if (!infobulle) {
      this.selectedFeature = allFeatures; // ajoute le feature en surcouche à la carte
      EventBus.$emit(EVENTS.SELECT_FEATURE, allFeatures);
      if (this.selectedFeature.length) {
        this.addToSelection(this.selectedFeature[0]);
      }
    }
  }

  addSelectionLayer() {
    // throw new Error('Method not implemented.');
  }

  addToSelection(feature:any):void{
    // throw new Error('Method not implemented.');
  }
}
