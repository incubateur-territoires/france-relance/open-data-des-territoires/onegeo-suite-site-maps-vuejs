/* eslint-disable prefer-destructuring */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-this-alias */
/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */
/* eslint-disable no-restricted-syntax */
/* eslint-disable import/no-named-as-default */
import LayerConfig from '@/models/map-context/LayerConfig';
import MapContext from '@/models/map-context/MapContext';
// eslint-disable-next-line import/extensions
import { register } from 'ol/proj/proj4.js';
import proj4 from 'proj4';
import LayerGroupConfig from '@/models/map-context/LayerGroupConfig';
import { CapabilitiesService, capabilitiesServiceInstance } from '@/services/CapabilitiesService';
import CapabilitiesParser from '@/models/CapabilitiesParser';
import axios from 'axios';
import { Observable, from, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { contexteServiceInstance, ContextService } from '@/services/ContextService';
import LayerAttribute from '@/models/map-context/LayerAttribute';
import EventBus, { EVENTS } from './EventBus';
import LayerManager from './LayerManager';

abstract class AbstractMapService {
    public config!: MapContext;

    contextService=contexteServiceInstance;

    layerManager = new LayerManager(this.contextService);

    constructor() {
      proj4.defs('EPSG:2154', '+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');
      proj4.defs('EPSG:3395', '+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs ');
      proj4.defs('EPSG:3944', '+proj=lcc +lat_1=43.25 +lat_2=44.75 +lat_0=44 +lon_0=3 +x_0=1700000 +y_0=3200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');
      proj4.defs('EPSG:3945', '+proj=lcc +lat_1=44.25 +lat_2=45.75 +lat_0=45 +lon_0=3 +x_0=1700000 +y_0=4200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');
      proj4.defs('EPSG:3946', '+proj=lcc +lat_1=45.25 +lat_2=46.75 +lat_0=46 +lon_0=3 +x_0=1700000 +y_0=5200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');
      proj4.defs('EPSG:3947', '+proj=lcc +lat_1=46.25 +lat_2=47.75 +lat_0=47 +lon_0=3 +x_0=1700000 +y_0=6200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');
      proj4.defs('EPSG:3948', '+proj=lcc +lat_1=47.25 +lat_2=48.75 +lat_0=48 +lon_0=3 +x_0=1700000 +y_0=7200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');
      proj4.defs('EPSG:3949', '+proj=lcc +lat_1=48.25 +lat_2=49.75 +lat_0=49 +lon_0=3 +x_0=1700000 +y_0=8200000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');
      register(proj4);
    }

    abstract getVectorFeatureAtPixel(pixel:any, layer:LayerConfig):any;

    abstract removeLayerFromMap(layer: LayerConfig):void;

    abstract addWMSLayer(layer: LayerConfig):void;

    abstract addWFSLayer(layer: LayerConfig):void;

    abstract addWMTSLayerFromCapabilities(layer: LayerConfig, data:any):void;

    abstract addVectorLayer(layer: LayerConfig):void;

    abstract addTMSLayer(layer: LayerConfig):void;

    abstract addVectorTileLayerFromCapabilities(layer: LayerConfig, data:any):void;

    abstract addOSMLayer(layer: LayerConfig):void;

    abstract addOtherLayerType(layer: LayerConfig):void;

    abstract sortByZIndexgetLayersAsArray(): LayerConfig[];

    abstract zoomToExtent(extent:number[]):void;

    abstract zoomToInitialExtent():void;

    abstract getMapExtent():any;

    initLayersFromConfig():void {
      const layers = this.config.getLayers();
      for (let i = 0; i < layers.length; i += 1) {
        const lay = layers[i];
        if (lay instanceof LayerGroupConfig) {
          const childs = lay.getAllChildLayers();
          for (let j = 0; j < childs.length; j += 1) {
            this.addLayer(childs[j]);
          }
        } else {
          this.addLayer(<LayerConfig>lay);
        }
      }
    }

    addLayer(layercfg: LayerConfig): void {
      this.computeZIndex(layercfg);

      if (layercfg.type === 'WMS') {
        this.addWMSLayer(layercfg);
      } else if (layercfg.type === 'WMTS') {
        this.addWMTSLayer(layercfg);
      } else if (layercfg.type === 'WFS') {
        this.addWFSLayer(layercfg);
      } else if (layercfg.type === 'VectorTile' && layercfg.protocol === 'TMS') {
        this.addTMSLayer(layercfg);
      } else if (layercfg.type === 'VectorTile') {
        this.addVectorTileLayer(layercfg);
      } else if (layercfg.type === 'Vector') {
        this.addVectorLayer(layercfg);
      } else if (layercfg.type === 'OSM') {
        this.addOSMLayer(layercfg);
      } else {
        this.addOtherLayerType(layercfg);
      }

      EventBus.$emit(EVENTS.LAYER_ADDED, layercfg);
    }

    addVectorTileLayer(layerconfig: any):void {
      const self = this;
      const url = layerconfig.getWmtsCapabilitiesUrl();
      capabilitiesServiceInstance.getCapabilities(url).subscribe(
        (data) => self.addVectorTileLayerFromCapabilities(layerconfig, data),
        (error) => {
          layerconfig.error = error.message;
          console.log(error);
        },
      );
    }

    /**
    * Ajoute une couche WMTS
    * @param layerconfig
    */
    addWMTSLayer(layerconfig: any) {
      const self = this;
      capabilitiesServiceInstance.getCapabilities(layerconfig.url)
        .subscribe(
          (data:any) => self.addWMTSLayerFromCapabilities(layerconfig, data),
          (error:any) => {
            layerconfig.error = error.message;
            console.log(error);
          },
        );
    }

    computeZIndex(layercfg: LayerConfig) {
      if (layercfg.layerIndex === undefined && this.sortByZIndexgetLayersAsArray().length > 0) {
        layercfg.layerIndex = this.sortByZIndexgetLayersAsArray()[0].layerIndex + 1;
      }
    }

    getOrderedLayerList(): LayerConfig[] {
      return this.config.getOrderedLayerList();
    }

    removeLayerFromList(layer: LayerConfig):void {
      this.config.removeLayerFromList(layer);
      EventBus.$emit(EVENTS.LAYER_ORDER_CHANGE);
    }

    removeGroupFromList(layerGroup: LayerGroupConfig):void {
      this.removeGroupFromListInternal(layerGroup);
      EventBus.$emit(EVENTS.LAYER_ORDER_CHANGE);
    }

    private removeGroupFromListInternal(layerGroup: LayerGroupConfig) {
      for (const child of layerGroup.childs.slice()) {
        if (child instanceof LayerGroupConfig) {
          this.removeGroupFromListInternal(child);
        } else {
          this.config.removeLayerFromList(child);
          this.removeLayerFromMap(<LayerConfig>child);
        }
      }
      this.config.removeLayerFromList(layerGroup);
    }

    checkBboxValidity(bbox:any):boolean {
      const valid = bbox[0] >= -180 && bbox[0] <= 180
        && bbox[1] >= -90 && bbox[1] <= 90
        && bbox[2] >= -180 && bbox[2] <= 180
        && bbox[3] >= -90 && bbox[3] <= 90;
      return valid;
    }

    getLayerBeingEdited(): LayerConfig|undefined {
      const editingLayerList = this.getOrderedLayerList().filter((x) => x.editing === true);
      if (editingLayerList.length === 1) {
        return editingLayerList[0];
      }
      return undefined;
    }

    getFeaturesTypes(layer: LayerConfig): Observable<any> {
      const headers:any = {};
      if (layer.authenticationInfos
        && layer.authenticationInfos.isFilled()) {
        headers.Authorization = layer.authenticationInfos.getBasicAutorisationValue();
      }
      if (layer.getDescribeFeatureUrl() !== '') {
        return from(axios.get(layer.getDescribeFeatureUrl(), { headers }))
          .pipe(
            map(
              (reponse) => {
                if (reponse.data.indexOf && reponse.data.indexOf('ExceptionText') > 0) {
                  console.log(reponse.data);
                  throw new Error('Impossible de récupérer lire la réponse');
                }

                return reponse.data;
              },
            ),
          );
      }
      if (layer.type === 'Vector' && layer.url !== undefined && layer.getLayerSource()) {
        const feats = layer.getLayerSource().getFeatures();
        if (feats.length > 0) {
          const properties = feats[0].getKeys().map((x:any) => ({ name: x, type: 'xsd:string' }));
          return of({ featureTypes: [{ properties }] });
        }
        return of(undefined);
      }
      if (layer.type === 'VectorTile' && layer.getLayerSource()) {
        const extent = this.getMapExtent();
        const feats = layer.getLayerSource().getFeaturesInExtent(extent);
        if (feats.length > 0) {
          const properties = Object.keys(feats[0].getProperties()).map((x:any) => ({ name: x, type: 'xsd:string' }));
          return of({ featureTypes: [{ properties }] });
        }
        return of(undefined);
      }
      return of(undefined);
    }

    setAttributeDefinition(layer:LayerConfig):void {
      if ((!layer.layerProperties || layer.layerProperties.length === 0) && layer.type !== 'Vector') {
        this.getFeaturesTypes(layer)
          .subscribe(
            (reponse: any) => {
              const result: any = reponse;
              if (result === 'WMTS') {
                console.log('pas de service WFS');
              } else if (result) {
                console.log('get layer attributes infos');
                if (result.featureTypes) {
                  layer.layerProperties = result.featureTypes[0].properties.map((x:any) => new LayerAttribute(x));
                  console.log(layer.layerProperties);
                  layer.targetNamespace = result.targetNamespace;
                  layer.targetPrefix = result.targetPrefix;
                  layer.layerProperties.forEach((field: any) => {
                    field.display = true;
                    field.editable = true;
                    if (field.type === 'gml:Point' || field.type === 'gml:MultiPoint') {
                      layer.geometryType = 'point';
                    }
                    if (field.type === 'gml:Polygon' || field.type === 'gml:MultiPolygon') {
                      layer.geometryType = 'polygone';
                    }
                    if (field.type === 'gml:LineString' || field.type === 'gml:MultiLineString') {
                      layer.geometryType = 'ligne';
                    }
                  });
                } else {
                  this.parseFeatureTypeFromXml(result, layer);
                }
                // this.updateProperties();
              }
            },
            (error: any) => {
              console.log('error');
              console.log(error);
              this.parseFeatureTypeFromXml(error.error.text, layer);
            },
          );
      }
    }

    parseFeatureTypeFromXml(xml:any, layer:LayerConfig):void {
      if (xml.indexOf('xsd:schema') > 0) {
        const parser = new CapabilitiesParser();
        const describe = parser.parseDescribeFeature(xml);
        const proper = describe['xsd:schema']['xsd:complexType']['xsd:complexContent']['xsd:extension']['xsd:sequence']['xsd:element'];
        layer.layerProperties = proper.map((x: any) => new LayerAttribute(x['@attributes']));
        layer.layerProperties.forEach((field: LayerAttribute) => {
          field.display = true;
        });
        if (describe['xsd:schema']['@attributes'] && describe['xsd:schema']['@attributes'].targetNamespace) {
          layer.targetNamespace = describe['xsd:schema']['@attributes'].targetNamespace;
          layer.targetPrefix = layer.targetNamespace.replace('http://', '');
        }
      } else if (xml.indexOf('schema') > 0) {
        const parser = new CapabilitiesParser();
        const describe = parser.parseDescribeFeature(xml);
        const proper = describe.schema.complexType.complexContent.extension.sequence.element;
        layer.layerProperties = proper.map((x: any) => new LayerAttribute(x['@attributes']));
        layer.layerProperties.forEach((field: LayerAttribute) => {
          field.display = true;
          field.type = `xsd:${field.type}`;
        });
        if (describe.schema['@attributes'] && describe.schema['@attributes'].targetNamespace) {
          layer.targetNamespace = describe.schema['@attributes'].targetNamespace;
          layer.targetPrefix = layer.targetNamespace.replace('http://', '');
        }
      }
    }
}
export default AbstractMapService;
