/* eslint-disable @typescript-eslint/no-this-alias */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */
import Filter from '@/models/map-context/filter/Filter';
import LayerConfig from '@/models/map-context/LayerConfig';
import TileState from 'ol/TileState';
import ContextService from './ContextService';

export default class LayerManager {
  nbtileLoading = 0;

  contextService!:ContextService;

  constructor(cs:ContextService) {
    this.contextService = cs;
  }

  addLoadingFunction(source: any, layerconfig: LayerConfig, crsfToken:string) :void{
    layerconfig.checkSourceAuthentification();

    const layercfg = layerconfig;
    const self = this;
    const loader = function customLoader(tile: any, src: string) {
      const maxUrlLength = 2000;
      /* if (!layercfg.authenticationInfos.isRequired() && src.length < maxUrlLength) {
        tile.getImage().src = src;
        return;
      } */
      const client = new XMLHttpRequest();
      let params;
      if (src.length < maxUrlLength) {
        client.open('GET', src);
      } else {
        const urlArray = src.split('?');
        let url = urlArray[0];
        if (layercfg.useProxy) {
          url = self.contextService.getProxyFiedUrl(url);
        }
        params = urlArray[1];
        client.open('POST', url, true);
        client.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        client.setRequestHeader('X-CSRFToken', crsfToken);
      }
      client.responseType = 'arraybuffer';
      if (layercfg.authenticationInfos.isFilled()) {
        client.setRequestHeader('Authorization', layerconfig.authenticationInfos.getBasicAutorisationValue());
      }

      client.onload = function onload() {
        if (this.status === 401) {
          layercfg.error = 'Erreur dans de chargement de la couche: Autorisation requise (401)';
          tile.error = layercfg.error;
          tile.setState(TileState.ERROR);
        } else if (this.status === 500) {
          layercfg.error = 'Erreur dans de chargement de la couche: Erreur (500)';
          tile.error = layercfg.error;
          tile.setState(TileState.ERROR);
        } else if (this.status === 404) {
          layercfg.error = 'Erreur dans de chargement de la couche: Non Trouvé (404)';
          tile.error = layercfg.error;
          tile.setState(TileState.ERROR);
        }
        const arrayBufferView = new Uint8Array(this.response);
        const blob = new Blob([arrayBufferView], { type: 'image/png' });
        const urlCreator = window.URL || (window as any).webkitURL;
        const imageUrl = urlCreator.createObjectURL(blob);
        tile.getImage().src = imageUrl;
      };
      client.addEventListener('error', (e) => {
        layercfg.useProxy = true;
        console.log('erreur');
        tile.setState(TileState.ERROR);
      });
      client.send(params);
    };

    if (source.setImageLoadFunction) {
      source.setImageLoadFunction(loader);
    } else if (layerconfig.type !== 'VectorTile' && !layerconfig.isWFS() && layerconfig.type !== 'Vector') {
      source.setTileLoadFunction(loader);
    }
  }

  addLoading(layerSource: LayerConfig, e: any) :void{
    this.nbtileLoading += 1;
    if (!e.target.nbtileLoading) {
      e.target.nbtileLoading = 0;
    }
    e.target.nbtileLoading += 1;
  }

  addLoaded(layerSource: LayerConfig, e: any) :void{
    if (layerSource.type === 'VectorTile' && layerSource.filter) {
      const filter = new Filter();
      filter.fromCQL(layerSource.filter);
      e.tile.setFeatures(e.tile.getFeatures().filter((x:any) => filter.evaluate(x)));
    }
    this.nbtileLoading -= 1;
    e.target.nbtileLoading -= 1;
  }

  addLoadError(layerSource: LayerConfig, e: any) :void{
    this.nbtileLoading -= 1;
    layerSource.error = 'Erreur dans de chargement de la couche';
    if (e.tile && e.tile.error) {
      layerSource.error = e.tile.error;
    }
    e.target.nbtileLoading -= 1;
  }

  addLoadingListener(layerSource: any, layerconfig: LayerConfig) :void{
    layerSource.on('tileloadstart',
      this.addLoading.bind(this, layerconfig));
    layerSource.on('tileloadend',
      this.addLoaded.bind(this, layerconfig));
    layerSource.on('tileloaderror',
      this.addLoadError.bind(this, layerconfig));
  }

  getVectorLoaderFunction(layerconfig:LayerConfig):any {
    const self = this;
    const vectorLoaderFunction = function vectorLoaderFunction(extent:any,
      resolution:any, projection:any, callback:any) {
      const url = layerconfig.getWFSUrl(extent, projection);
      const xhr = new XMLHttpRequest();
      self.nbtileLoading += 1;
      xhr.open('GET', url);
      if (layerconfig.hasAuthenticationInfos()) {
        xhr.setRequestHeader('Authorization', layerconfig.authenticationInfos.getBasicAutorisationValue());
      }
      const onError = function onError() {
        self.nbtileLoading -= 1;
        layerconfig.getLayerSource().removeLoadedExtent(extent);
      };
      xhr.onerror = onError;
      xhr.onload = function onload() {
        self.nbtileLoading -= 1;
        if (xhr.status === 200) {
          const features = layerconfig.getLayerSource().getFormat().readFeatures(xhr.responseText);
          layerconfig.getLayerSource().addFeatures(features);
          callback(features);
        } else {
          onError();
        }
      };
      xhr.send();
    };
    return vectorLoaderFunction;
  }
}
