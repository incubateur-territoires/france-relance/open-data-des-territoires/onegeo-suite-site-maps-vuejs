/* eslint-disable */
import { View, Map, Overlay } from 'ol';

import { WFS, MVT,GeoJSON } from 'ol/format';
import WMTS, { optionsFromCapabilities } from 'ol/source/WMTS';

import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import MapContext from '@/models/map-context/MapContext';
import LayerConfig from '@/models/map-context/LayerConfig';
import MapUtils from '@/models/map-context/MapUtils';
import BaseLayer from 'ol/layer/Base';

import { transform, get, transformExtent } from 'ol/proj.js'
import { boundingExtent } from 'ol/extent';
import { register } from 'ol/proj/proj4.js'
import proj4 from 'proj4';
import { defaults, MousePosition, } from 'ol/control';
import axios, { AxiosResponse } from 'axios';
import {
  Observable, of, pipe, from,
} from 'rxjs';
import {
  map,
} from 'rxjs/operators';
import { fromLonLat, toLonLat } from 'ol/proj.js';
import WMTSCapabilities from 'ol/format/WMTSCapabilities';
import LayerGroupConfig from '@/models/map-context/LayerGroupConfig';
import AbstractLayerTreeElement from '@/models/map-context/AbstractLayerTreeElement';
import EventBus, { EVENTS } from './EventBus';
import { CapabilitiesService, capabilitiesServiceInstance } from '@/services/CapabilitiesService';
import Plugin from '@/models/plugin/Plugin';
import { Coordinate, CoordinateFormat, format } from 'ol/coordinate';

import { contexteServiceInstance, ContextService } from '@/services/ContextService';
import Filter from '@/models/map-context/filter/Filter';
import LayerManager from './LayerManager';
import Projection from 'ol/proj/Projection';
import { TileCoord } from 'ol/tilecoord';
import AbstractMapService from './AbstractMapService';
import CesiumSelectionService from './CesiumSelectionService';

declare var Cesium: any;

class CesiumMapService extends AbstractMapService{
  
  addTMSLayer(layer: LayerConfig): void {
    throw new Error('Method not implemented.');
  }
  zoomToInitialExtent(): void {
    throw new Error('Method not implemented.');
  }
  zoomToExtent(extent: number[]): void {
    throw new Error('Method not implemented.');
  }
  layers: any = {};

  viewer: any;

  moveScaleFactor = 1;
  
  contextService=contexteServiceInstance;

  wmtsResult: any

  public idLayers_count: number = 1;

  public currentZoom: number = 0;

  public map_projection_code = 'EPSG:3857';

  public initLocation = [5.1, 47.29];

  public initZoom = 9;

  currentScale: number = 0;

  public mousePosition!:MousePosition;

  selectionService!:CesiumSelectionService;
  
  selectedEntity:any;

  constructor(){
    super();
  }

  initMap(currentAppConfig: MapContext) {
    //Cesium.Ion.defaultAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI1YTBlZTFmNi1mNjBjLTQ5MzUtOTg0Zi04Y2Y0Mjk2ZTQ2OTgiLCJpZCI6NDU2NDksImlhdCI6MTYxNTI4Njg2MX0.QJjcfV4WJwX2Ht111wFsFYAGlD2nsKIK9Z9_xPLuYgM";
    this.config = currentAppConfig;

    const options = {
      animation: false,
      homeButton: false,
      baseLayerPicker : false,
      geocoder:false,
      infoBox:false,
      timeline: false,
      sceneModePicker: false,
      imageryProvider: new Cesium.WebMapTileServiceImageryProvider({
        url: 'https://wxs.ign.fr/essentiels/geoportail/wmts',
        layer: 'ORTHOIMAGERY.ORTHOPHOTOS',
        style: 'normal',
        format: 'image/jpeg',
        tileMatrixSetID: 'PM',
      }),
    };

    const timelineParam=currentAppConfig.cesiumParameters['timeline'];
    if( timelineParam!=undefined ){
      options.timeline=timelineParam;
    }
    this.viewer = new Cesium.Viewer('cesiumContainer',options );
    const shadowsParam=currentAppConfig.cesiumParameters['shadows'];
    if( shadowsParam!=undefined ){
      this.viewer.shadows=shadowsParam;
    }

    
    if(this.contextService.configuration['3d'] 
      && this.contextService.configuration['3d'].terrainProviderUrl 
      && this.contextService.configuration['3d'].terrainProviderUrl.length>0) {
        var terrainProvider = new Cesium.CesiumTerrainProvider({
          url: this.contextService.configuration['3d'].terrainProviderUrl,
        });
        //url: 'http://51.210.185.61/datas/tiles',
        //this.viewer.extend(Cesium.viewerCesiumInspectorMixin);
        this.viewer.scene.terrainProvider = terrainProvider;
    }

    this.viewer.scene.globe.depthTestAgainstTerrain = true;

    var west = 0.57;
    var south = 42.78;
    var east = 0.6;
    var north = 42.8;
    let rectangle = Cesium.Rectangle.fromDegrees(west, south, east, north);

    this.postInit();
    if(this.config.bbox) {
        const bbox=this.config.bbox;
        let ext = boundingExtent([[bbox.minx, bbox.miny], [bbox.maxx, bbox.maxy]]);
        let repojectedExtent = transformExtent(ext, bbox.srs, "EPSG:4326");
        console.log(repojectedExtent);
        rectangle = Cesium.Rectangle.fromDegrees(repojectedExtent[0], repojectedExtent[1], repojectedExtent[2], repojectedExtent[3]);
    }
    else {
      // A revoir !! il aurait mieux fallu avoir une bbox en config plutot qu'un centre + niveau de zoom
      let altitude = Math.pow(2.0,(29-this.config.zoom));
      this.viewer.camera.setView({
        destination : Cesium.Cartesian3.fromDegrees(this.config.coords[0], this.config.coords[1], altitude)
      });
    }
    
    var scene = this.viewer.scene;
    scene.globe.depthTestAgainstTerrain = true;

    
    // Color a feature yellow on hover.
    //viewer.extend(Cesium.viewerCesium3DTilesInspectorMixin);
    EventBus.$emit(EVENTS.MAP_INIT_END);
    
  }
  
  addOtherLayerType(layer: LayerConfig): void {
    /*if(layer.type=='3dmodel'){
      this.addModel(layer);
    }
    else */
    if (layer.type === '3DTiles') {
      this.addTileSetLayer(layer,0);
    }
    else if (layer.type === '3DTilesPointCloud') {
      this.addTileSetPointCloudLayer(layer);
    }
  }
 

  addTileSetPointCloudLayer(layer: any) {
    let id = layer.layername;
    //var tileset:any = this.addTileSetLayer(layer.url, -80);
    var tileset:any = this.addTileSetLayer(layer, 0);
    //tileset.show = false;
    let pointSize = 2.0;
    let custom_color = '${COLOR}';
    if (layer.pointSize != undefined) {
      pointSize = layer.pointSize;
    }
    if (layer.color != undefined) {
      custom_color = layer.color;
    }
    //tileset.pointCloudShading.attenuation = true;
    //tileset.pointCloudShading.eyeDomeLighting = true;
    this.layers[id] = tileset;
    console.log(tileset);
    tileset.style = new Cesium.Cesium3DTileStyle({
      //color : "color('#ff0000')",
      //color:"${COLOR}*color('purple')",
      color: custom_color,
      //show: "${POSITION_ABSOLUTE}.z > 5 && ${POSITION_ABSOLUTE}.z <750",
      //color: "vec4(${POSITION}, 1.0)",
      //color: "color('red') * ((${POSITION_ABSOLUTE}.z-0)/400) + color('blue') * (1.0 - ((${POSITION_ABSOLUTE}.z-0)/400))",
      //color:  'rgb(max(exp2(${POSITION}.z),0.1) * 255, max(exp(${POSITION}.z),0.1) * 255, 55)',
      //color : "rgb(floor(${POSITION}[0]) * 255, ceil(${POSITION}[1]) * 255, round(${POSITION}[2]) * 255)",
      pointSize: pointSize,
    });
    //tileset.style = new Cesium.Cesium3DTileStyle({show: "${POSITION}.z > 5",pointSize: 1.0});
    //tileset.style = new Cesium.Cesium3DTileStyle({show: "${POSITION}.z > 7",pointSize: 5.0});
    /*tileset.style = new Cesium.Cesium3DTileStyle({
      color: "rgb(${Intensity}, ${Intensity}, ${Intensity})",
    });*/
    /*tileset.style = new Cesium.Cesium3DTileStyle({
      color: {
        conditions: [
          ["${Classification} === 'roof1'", "color('#004FFF', 0.5)"],
          ["${Classification} === 'towerBottom1'", "color('#33BB66', 0.5)"],
          ["${Classification} === 'towerTop1'", "color('#0099AA', 0.5)"],
          ["${Classification} === 'roof2'", "color('#004FFF', 0.5)"],
          ["${Classification} === 'tower3'", "color('#FF8833', 0.5)"],
          ["${Classification} === 'tower4'", "color('#FFAA22', 0.5)"],
          ["true", "color('#FFFF00', 0.5)"],
        ],
      },
    });*/
    return tileset;
  }

  addTileSetLayer(layer: any, layer_offset: any) {
    var tileset = new Cesium.Cesium3DTileset({
      url: layer.url,

      /*dynamicScreenSpaceError : true,
      debugColorizeTiles: true,
      dynamicScreenSpaceErrorDensity : 0.00278,
      dynamicScreenSpaceErrorFactor : 4.0,
      dynamicScreenSpaceErrorHeightFalloff : 0.25,
       debugShowBoundingVolume:true*/
    });
    let self = this;
    tileset.readyPromise
      .then(function (tileset: {
        boundingSphere: { center: any; radius: number };
        modelMatrix: any;
      }) {
        if (layer_offset) {
          var cartographic = Cesium.Cartographic.fromCartesian(
            tileset.boundingSphere.center
          );
          var surface = Cesium.Cartesian3.fromRadians(
            cartographic.longitude,
            cartographic.latitude,
            0.0
          );
          var offset = Cesium.Cartesian3.fromRadians(
            cartographic.longitude,
            cartographic.latitude,
            layer_offset
          );
          var translation = Cesium.Cartesian3.subtract(
            offset,
            surface,
            new Cesium.Cartesian3()
          );
          tileset.modelMatrix = Cesium.Matrix4.fromTranslation(translation);
        }
        self.viewer.scene.primitives.add(tileset);
        layer.setCesium_Layer(tileset);
      })
      .otherwise(function (error: any) {
        console.log(error);
      });

    return tileset;
  }


  destroyMap() {
    this.clearLayers();
    (<any>this.viewer) = null;
  }

  updateConfigLocationFromMap(){
    console.log("TODO");
  }

  clearLayers() {
    let layers = this.viewer.imageryLayers;
    for (var i = 0; i < layers.length; i++) {
      var layer = layers[i];
      this.viewer.imageryLayers.remove(layer);
    }
  }

  postInit() {
    this.computeCurrentScale();
    this.selectionService=new CesiumSelectionService(this);
    
    this.viewer.camera.moveEnd.addEventListener(this.onZoom.bind(this));
    this.initLayersFromConfig();
  }
 
  computeCurrentScale(){
    const cameraPosition = this.viewer.scene.camera.positionWC;
    const ellipsoidPosition = this.viewer.scene.globe
      .ellipsoid.scaleToGeodeticSurface(cameraPosition);
      const distance = Cesium.Cartesian3.magnitude(Cesium.Cartesian3
        .subtract(cameraPosition, ellipsoidPosition, new Cesium.Cartesian3()));
    this.currentScale = distance;
  }

  onZoom(event:any) {
    this.computeCurrentScale();
    console.log(this.currentScale)
    var scratchRectangle = new Cesium.Rectangle();
    var rect = this.viewer.camera.computeViewRectangle(this.viewer.scene.globe.ellipsoid,
        scratchRectangle);
    const mapExtent = [
      Cesium.Math.toDegrees(rect.west).toFixed(4),
      Cesium.Math.toDegrees(rect.south).toFixed(4),
      Cesium.Math.toDegrees(rect.east).toFixed(4),
      Cesium.Math.toDegrees(rect.north).toFixed(4)
    ];
    
    this.onMoveEnd(mapExtent,this.viewer.camera);
  }

  onMoveEnd(bbox:any,camera:any) {
    const url = new URL(window.location.href);
    const hash = window.location.hash.substr(1);
    const hashSplitted = window.location.hash.substr(1).split('?');
    let hashParams:any = {};
    if (hashSplitted.length > 1) {
      hashParams = hashSplitted[1].split('&').reduce((res:any, item) => {
        const parts = item.split('=');
        res[parts[0]] = parts[1];
        return res;
      }, {});
    }
    hashParams.bbox = `${bbox[0]},${bbox[1]},${bbox[2]},${bbox[3]}`;
    hashParams.camera_pos = `${camera.position.x},${camera.position.y},${camera.position.z}`;
    hashParams.camera_heading = camera.heading;
    hashParams.camera_pitch = camera.pitch;
    hashParams.camera_roll = camera.roll;
    const stringHashParams = Object.keys(hashParams).map((key) => [key, hashParams[key]].join('=')).join('&');
    url.hash = `#${hashSplitted[0]}?${stringHashParams}`;

    window.history.replaceState('', '', url.href);
  }

  setCameraView(position:number[],heading:number,pitch:number,roll:number){
    this.viewer.camera.setView({
      destination: new Cesium.Cartesian3(position[0], position[1], position[2]),
      orientation : {
        heading:heading,
        pitch:pitch,
        roll:roll
    }
    });
  }

  zoomToBboxWGS84(bbox:number[]){

    let ext = boundingExtent([[bbox[0], bbox[1]], [bbox[2], bbox[3]]]);
    let rectangle = Cesium.Rectangle.fromDegrees(ext[0], ext[1], ext[2], ext[3]);
    
    this.viewer.camera.setView({
      destination: rectangle,
      orientation: {
        heading : Cesium.Math.toRadians(0.0), // east, default value is 0.0 (north)
        pitch : Cesium.Math.toRadians(-60),    // default value -90.0  (looking down)
        roll : 0.0                             // default value 0.0
    }
    });
  }

  zoomToLayer(layer:LayerConfig) { 
    const [minx, miny, maxx, maxy] = <any>layer.boundingBoxEPSG4326;
    let ext = boundingExtent([[minx, miny], [maxx, maxy]]);
    let rectangle = Cesium.Rectangle.fromDegrees(ext[0], ext[1], ext[2], ext[3]);
    
    this.viewer.camera.setView({
      destination: rectangle,
    });
  }

  zoomTo(location:Coordinate, zoomlevel:number, lon?:number, lat?:number) {
    if (lon && lat) {
      location = [+lon, +lat];
    }
    this.viewer.camera.flyTo({
      destination : Cesium.Cartesian3.fromDegrees(location[0], location[1], 5000.0),
      duration : 20.0
    }); 
    // TODO 
    // this.map.getView().setCenter(transform(location, 'EPSG:4326', 'EPSG:3857'));
    // this.map.getView().setZoom(zoomlevel);
  }

  addOverlay(loc:Coordinate) {
   console.log('not implemented')
  }

  initLocaleConfig() {
    this.map_projection_code = this.config.projection;
    this.initZoom = this.config.zoom;
    this.initLocation = this.config.coords;
  }

  addOSMLayer(layercfg:LayerConfig){
    let osm =new Cesium.OpenStreetMapImageryProvider();
    const imageryLayer = this.viewer.imageryLayers.addImageryProvider(osm);
    layercfg.setCesium_Layer(imageryLayer);
    layercfg.baseLayer = true;
    this.setZlevelOpacityAndResolutions(imageryLayer, layercfg);
  }

  addVectorLayer(layerconfig: LayerConfig): LayerConfig {
    let self = this;
    if (layerconfig.jsonData && layerconfig.jsonData.length > 0) {
      var feats = (new GeoJSON()).readFeatures(layerconfig.jsonData);
      // var promise = Cesium.GeoJsonDataSource.load(url);
    } else if (layerconfig.url != undefined) {
      fetch(layerconfig.url).then((data)=>{
        console.log(data);
        data.text().then((data) => {
        var feats = (new GeoJSON()).readFeatures(data);
        console.log(feats)
      });
      })
      var promise = Cesium.GeoJsonDataSource.load(layerconfig.url);
      promise
      .then(function (dataSource: any) {
        console.log(dataSource);
        let id = layerconfig.layername;
        self.viewer.dataSources.add(dataSource);
          dataSource.show = true;
          self.layers[id] = dataSource;
          layerconfig.setCesium_Layer(dataSource);
      });

    }

    
    return layerconfig;
  }

  addWFSLayer(layerconfig: LayerConfig) {
    // TODO 
    return layerconfig;
  }



  addWMTSLayerFromCapabilities(layerconfig: LayerConfig, wmtsCapabilities:any) {
    let self = this;
    let layername = layerconfig.layername;
    let id = layerconfig.id;

    if (this.getLayersById(id) == null) {
      let options:any;
      try {
        if(layerconfig.outputFormat){
          options = optionsFromCapabilities(wmtsCapabilities, {
            layer: layername,
            format : layerconfig.outputFormat,
          });
        }
        else{
          options = optionsFromCapabilities(wmtsCapabilities, {
            layer: layername,
          });
        }
      }
      catch (e:any) {
        console.log(e);
        if (e.message == "projection is null") {
          layerconfig.error = "Projection non reconnue";
        }
        else {
          layerconfig.error = "Problème d'analyse du getCapabilities";
        }
        return;
      }

      if (layerconfig.ignoreUrlInCapabiltiesResponse) {
        var searchMask = "request(=|%3D)getCapabilities";
        var regEx = new RegExp(searchMask, "ig");
        var replaceMask = "";
        options.urls[0] = layerconfig.url.replace(regEx, replaceMask);
      }
      options.attributions = layerconfig.getAttribution();
      options.crossOrigin= 'anonymous';
      // var layerSource = new WMTS(/** @type {!olx.source.WMTSOptions} */(options));

      // this.layerManager.addLoadingFunction(layerSource, layerconfig);
      // this.layerManager.addLoadingListener(layerSource, layerconfig);
      // let layerOl = new TileLayer({
      //  source: layerSource
      // });
      console.log(options);
      let cesiumLayer = new Cesium.WebMapTileServiceImageryProvider({
        url: options.urls[0],
        layer: options.layer,
        style: options.style,
        format: options.format,
        tileMatrixSetID: options.matrixSet,
        tileMatrixLabels:options.tileGrid.matrixIds_
      });
      var imageryLayer = this.viewer.imageryLayers.addImageryProvider(cesiumLayer);
      this.setZlevelOpacityAndResolutions(imageryLayer, layerconfig);
      layerconfig.setCesium_Layer(imageryLayer);
      //self.map.addLayer(layerOl);

    }
    else {
      (<any>layerconfig).ol_Layer = this.getLayersById(id);
      this.setZlevelOpacityAndResolutions(layerconfig.getCesiumLayer(), layerconfig);
    }
    layerconfig.getCesiumLayer().show= layerconfig.isLayerVisible();
    return layerconfig
  }



  getMapExtent() {
     // TODO
     return [];
  }
  getMapBoundsWGS84() {
    // TODO
    return  [];
  }

  addVectorTileLayerFromCapabilities(layerconfig: LayerConfig, wmtsCapabilities:any) {
    // TODO 

    return layerconfig;
  }

  sortByZIndexgetLayersAsArray(): LayerConfig[] {
    let clonedList = this.getOrderedLayerList().map((x) => x);
    clonedList.sort(function (a, b) {
      return a.getOlLayer() && b.getOlLayer() && a.getOlLayer().getZIndex() - b.getOlLayer().getZIndex();
    })
    clonedList.reverse();

    return clonedList;
  }

  
  removeLayerFromMap(layer: LayerConfig) {
    if (layer.getCesiumLayer()) {
      // TODO remove layer from cesium this.viewer.removeLayer(layer.getCesiumLayer());
      this.viewer.imageryLayers.remove(layer.getCesiumLayer()); 
      delete layer.cesium_Layer;
    }
  }

  getFeaturesTypes(layer: LayerConfig): Observable<any> {
    const headers:any = {};
    if (layer.authenticationInfos
      && layer.authenticationInfos.isFilled()) {
      headers.Authorization = layer.authenticationInfos.getBasicAutorisationValue();
    }
    if (layer.getDescribeFeatureUrl()!== '' ) {
      return from(axios.get(layer.getDescribeFeatureUrl(),{ headers}))
        .pipe(
          map(
            (reponse)=>{
              if(reponse.data.indexOf && reponse.data.indexOf('ExceptionText')>0){
                console.log(reponse.data);
                throw 'Impossible de récupérer lire la réponse';
              }


              return reponse.data;
            }));
    }
    else if (layer.type == 'Vector' && layer.url != undefined && layer.getLayerSource()) {
      let feats = layer.getLayerSource().getFeatures();
      if (feats.length > 0) {
        let properties = feats[0].getKeys().map((x:any) => { return { "name": x, "type": 'xsd:string' } });
        return of({ "featureTypes": [{ "properties": properties }] });
      }
      return of(undefined);
    }
    return of(undefined);
  }

  addWMSLayer(layerconfig: LayerConfig) {

    if (this.getLayersById(layerconfig.id) == null) {

      let urlwms = '';
      if (layerconfig.url != undefined && layerconfig.url != "") {
        urlwms = layerconfig.url;
      }
      var searchMask = "request(=|%3D)getCapabilities";
      var regEx = new RegExp(searchMask, "ig");
      var replaceMask = "";

      urlwms = urlwms.replace(regEx, replaceMask);


      let projection = this.map_projection_code;
      if (layerconfig.projection) {
        projection = layerconfig.projection;
      }

      let attributions = layerconfig.getAttribution();

      var imageryLayers = this.viewer.imageryLayers;

      var wmsLayer = new Cesium.WebMapServiceImageryProvider({
        url: urlwms,
        layers: layerconfig.layername,
        credit:attributions,
        getFeatureInfoFormats:[new Cesium.GetFeatureInfoFormat('xml', 'application/vnd.ogc.gml')],
        parameters: {
          transparent: true,
          format: 'image/png',
        },
      });

      var imageryLayer = imageryLayers.addImageryProvider(wmsLayer);
      this.layers[layerconfig.layername] = imageryLayer;
      

      //this.layerManager.addLoadingFunction(source, layerconfig);

      //this.layerManager.addLoadingListener(source, layerconfig);
      this.setZlevelOpacityAndResolutions(imageryLayer, layerconfig);
      layerconfig.setCesium_Layer(imageryLayer);

      if (layerconfig.styles) {
        wmsLayer.parameters.updateParams({ 'STYLES': layerconfig.styles[0].style });
        layerconfig.selectedStyle = layerconfig.styles[0];
      }
      if (layerconfig.filter) {
        wmsLayer.parameters.updateParams({ 'CQL_FILTER': layerconfig.getFilterValue() });
      }
      if (layerconfig.useSldBody) {
        let sld = layerconfig.styletoSld();
        wmsLayer.parameters.updateParams({ 'SLD_BODY': sld });
      }

      //this.map.addLayer(layerconfig.getOlLayer());

    }

    const layer = this.getLayersById(layerconfig.id);
    if (layer != null)
      imageryLayer.show = layerconfig.isLayerVisible();

  }

  getLayersById(id: string): BaseLayer | null {
    let layerList = this.getOrderedLayerList().filter(function (layer) {
      return layer.id === id;
    });
    if (layerList != undefined && layerList.length > 0) {
      return layerList[0].getCesiumLayer();
    }
    else {
      return null;
    }

  }

  changeLayerOrder(source: AbstractLayerTreeElement, target: AbstractLayerTreeElement, location: string) {
    if (source instanceof LayerGroupConfig && source.getAllChilds().indexOf(target) > 0) {
      console.error("cannot drop in child");
      return;
    }
    this.config.changeLayerOrder(source, target, location);

    this.computeZIndexAndVisibility();
  }


  computeZIndexAndVisibility() {
    let layers = this.config.getOrderedLayerList().reverse();
    // Sauvegarde du zIndex de la couche.
    for (let i = 0; i < layers.length; i++) {
      layers[i].layerIndex = i + 1;
      if (layers[i].getCesiumLayer() === undefined) {
        console.error("Cesium layer non présent", layers[i])
      }
      //layers[i].getCesiumLayer().setZIndex(i + 1);
      if (!layers[i].baseLayer) {
        layers[i].getCesiumLayer().show = layers[i].isLayerVisible();
      }
    }
    for (let i = 0; i < layers.length; i++) {
      this.setZlevelOpacityAndResolutions(layers[i].getCesiumLayer(),layers[i]);
      
    }
  }

  setZlevelOpacityAndResolutions(layer: any, layerconfig: LayerConfig) {
    //layer.setZIndex(layerconfig.layerIndex);
    console.log(layerconfig);
    
    let currentIndex=this.viewer.imageryLayers.indexOf(layer);
    if(currentIndex >=0 && currentIndex < layerconfig.layerIndex){
      if(layerconfig.layerIndex >= this.viewer.imageryLayers.length){
        this.viewer.imageryLayers.raiseToTop(layer);
      }
      else{
        while(currentIndex < layerconfig.layerIndex){
          this.viewer.imageryLayers.raise(layer);
          currentIndex=this.viewer.imageryLayers.indexOf(layer);
        }
      }

    }
    else if(currentIndex > layerconfig.layerIndex){
      while(currentIndex > layerconfig.layerIndex){
        this.viewer.imageryLayers.lower(layer);
        currentIndex=this.viewer.imageryLayers.indexOf(layer);
      }
    } 
    
    /*
    const maxResolution = this.getMinMaxResolutionInfos(layerconfig).maxResolution;
    if (maxResolution) {
      layer.setMaxResolution(maxResolution);
    }

    const minResolution = this.getMinMaxResolutionInfos(layerconfig).minResolution;
    if (minResolution) {
      layer.setMinResolution(minResolution);
    }

    if (layerconfig.opacity) {
      layer.alpha = layerconfig.opacity;
      layer.setOpacity(layerconfig.opacity);
    }
    */
  }

  getMinMaxResolutionInfos(layerconfig: LayerConfig) {

    var maxResolution = undefined;
    if (layerconfig.maxScaleDenominator) {
      maxResolution = MapUtils.getResolutionFromScale(layerconfig.maxScaleDenominator, "m");
    }
    var minResolution = undefined;
    if (layerconfig.minScaleDenominator) {
      minResolution = MapUtils.getResolutionFromScale(layerconfig.minScaleDenominator, "m");
    }
    return {
      "maxResolution": maxResolution,
      "minResolution": minResolution
    };
  }
  
  updateLayer(layer: LayerConfig) {
    layer.updateStyle();

    //TODO adaptations cesium
    if (layer.authenticationInfos && layer.getOlLayer()) {
      this.layerManager.addLoadingFunction(layer.getLayerSource(), layer, this.contextService.getCsrfToken());
      let key = layer.url;
      if (layer.authenticationInfos.store) {
        this.contextService.addAuthInfo(key, layer.authenticationInfos);
      }
      else {
        this.contextService.removeAuthInfo(key);
      }
    }

    if (layer.Attribution && layer.getOlLayer()) {
      layer.getLayerSource().setAttributions(layer.getAttribution())
    }
    if (layer.getOlLayer()) {
      this.setZlevelOpacityAndResolutions(layer.getOlLayer(), layer);
    }
    let bbox = layer.boundingBoxEPSG4326;
    if (layer.maxScaleDenominator && layer.minScaleDenominator && layer.maxScaleDenominator < layer.minScaleDenominator) {
      return {
        "success": false,
        "message": "L'échelle maximum de rendu doit être supérieure à l'échelle minimum."
      }

    }
    if (this.checkBboxValidity(bbox)) {

      //Pour WMS il faut mettre à jour la source, pour WFS pas besoin
      if (layer.type == "WMS" && layer.getLayerSource().updateParams) {
        layer.getLayerSource().updateParams({ 'LAYERS': layer.layername });

        if (layer.filter)
          layer.getLayerSource().updateParams({ 'CQL_FILTER': layer.getFilterValue() });
        else
          layer.getLayerSource().updateParams({ 'CQL_FILTER': undefined });
      }
      else {
        console.log("refresh WFS or VectorTile layer");
        if (layer.getOlLayer()!== undefined && (layer.type == "VectorTile" || layer.isWFS())) {
          layer.getLayerSource().clear();
          layer.getLayerSource().refresh();
        }

      }
      return { "success": true };

    } else {
      return {
        "success": false,
        "message": "Les coordonnées doivent respecter les intervalles [-90; 90] pour minX et maxX, [-180; 180] pour minY et maxY."
      }
    }
  }



  getVectorFeatureAtPixel(pixel:any,layer:LayerConfig){
    return [];
  }
}
const cesiumMapServiceInstance = new CesiumMapService();
export default CesiumMapService;
export { CesiumMapService, cesiumMapServiceInstance }
