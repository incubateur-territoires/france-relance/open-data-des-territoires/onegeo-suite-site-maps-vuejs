/* eslint-disable class-methods-use-this */
/* eslint-disable @typescript-eslint/no-this-alias */
import PluginConfig from '@/models/plugin/PluginConfig';
import { PluginJsonConfig } from '@/models/plugin/PluginJsonConfig';
import { ParameterType } from '@/models/plugin/ParameterType';
import PluginParameter from '@/models/plugin/parameter/PluginParameter';
import { PluginLocation } from '@/models/plugin/PluginLocation';
import Plugin from '@/models/plugin/Plugin';
import PluginSelectParameter from '@/models/plugin/parameter/PluginSelectParameter';
import axios, { AxiosResponse } from 'axios';
import { IPluginParameter } from '@/models/plugin/IPluginParameter';
import { EventBus, EVENTS } from './EventBus';

class PluginService {
    plugins: Plugin[] =
    [
      // ----- Navigation -------
      new Plugin('navigation',
        'Navigation',
        'Affichage des boutons de navigation',
        'NavigationPlugin',
        [
          new PluginParameter<boolean>('zoomInOut', 'Boutons Zoom + et -', ParameterType.Boolean, true),
          new PluginParameter<boolean>('zoomToMaxExtent', 'Bouton Vue Globale', ParameterType.Boolean, true),
        ],
        new PluginConfig({ isActive: true, position: PluginLocation.TopRight })),
      // ----- Echelle -------
      new Plugin('scale',
        'Echelle',
        'Affichage de l\'échelle de la carte',
        'ScalePlugin',
        [
          new PluginParameter<boolean>('mode', 'Mode ScaleBar', ParameterType.Boolean, false),
          new PluginParameter<number>('nbBars', 'Nombre de Barres', ParameterType.Number, 2),
        ],
        new PluginConfig({ isActive: true, position: PluginLocation.BottomLeft })),
      // ----- Liste d'Echelles -------
      new Plugin('scale-list',
        'Liste d\'Echelle',
        'Affichage d\'une liste d\'échelles',
        'ScaleListPlugin',
        [
        ],
        new PluginConfig({ isActive: true, position: PluginLocation.BottomLeft })),
      // ----- Coordonnées de la souris -------
      new Plugin('coordonnes',
        'Coordonnées',
        'Affichage des coordonnées de la position de la souris.',
        'MousePositionPlugin',
        [
          new PluginSelectParameter<string>('displayProj', 'Projection', 'EPSG:2154',
            [
              { text: 'EPSG:2154', value: 'EPSG:2154' },
              { text: 'EPSG:4326', value: 'EPSG:4326' },
              { text: 'EPSG:3857', value: 'EPSG:3857' }]),
          // new PluginParameter<string>('displayProj',
          // 'Projection', ParameterType.Text, 'EPSG:2154'),
          new PluginParameter<string>('format', 'Format', ParameterType.Text, 'X:{x} Y:{y} (Lambert 93)'),
          new PluginParameter<number>('precision', 'Nombre de chiffres significatifs', ParameterType.Number, 2),
        ],
        new PluginConfig({ isActive: true, position: PluginLocation.BottomLeft })),
      // ----- Recherche de lieux -------
      new Plugin('recherche-lieux',
        'Recherche de lieux',
        'Rechercher et localiser des adresses et lieux-dits.',
        'ApiAdressePlugin',
        [new PluginParameter<number>('result-count', 'Nombre de résultats', ParameterType.Number, 5)],
        new PluginConfig({ isActive: true, position: PluginLocation.TopRight })),
      // ----- Indicateur de chargement des tuiles -------
      new Plugin('loading-infos',
        'Indicateur de chargement',
        'Affiche une indication lorsque la carte n est pas complètement chargée.',
        'LoadingInfosPlugin',
        [],
        new PluginConfig({ isActive: true, position: PluginLocation.BottomRight })),
      // ----- Gestionnaire de couches -------
      new Plugin('layerList',
        'Couches de données',
        'Afficher le gestionnaire de couches',
        'LayerListPanel',
        [],
        new PluginConfig({ isActive: true, position: PluginLocation.LeftToolBar, icon: 'layers' })),
      // ----- Légende -------
      new Plugin('legend',
        'Légende',
        'Afficher une légende',
        'LegendPlugin',
        [],
        new PluginConfig({ isActive: true, position: PluginLocation.LeftToolBar, icon: 'list-stars' })),
      // ----- Impression -------
      new Plugin('print',
        'Impression',
        'Imprimer la carte en pdf ou en png',
        'PrintPanelPlugin',
        [],
        new PluginConfig({ isActive: true, position: PluginLocation.LeftToolBar, icon: 'printer' })),
      // ----- Fiche Info -------
      new Plugin('feature-infos',
        'Fiche Information',
        'Affiche une fiche info lorsqu\'on clique sur un objet de la carte',
        'FeatureInfosPanel',
        [],
        new PluginConfig({ isActive: true, position: PluginLocation.LeftToolBar, icon: 'file-earmark-text' })),
      // ----- Mesure de distances -------
      new Plugin('measure',
        'Mesure de distances/surfaces',
        'Afficher les boutons de mesures de distances/surfaces.',
        'MesurePlugin',
        [
          new PluginParameter<boolean>('distances', 'Mesure de distances', ParameterType.Boolean, true),
          new PluginParameter<boolean>('surfaces', 'Mesure de surfaces', ParameterType.Boolean, true),
        ],
        new PluginConfig({ isActive: true, position: PluginLocation.TopLeft })),
      // ----- Permalien -------
      new Plugin('permalink',
        'Permaliens',
        'Affiche un permalien pour partager la carte',
        'PermalinkPlugin',
        [
        ],
        new PluginConfig({ isActive: true, position: PluginLocation.LeftToolBar, icon: 'share-fill' })),
    ];

    constructor() {
      console.log('pluginService loaded');
      this.loadCustomPlugins().then((result) => {
        console.log(result.data);
        result.data.forEach((pluginCfg:any) => {
          const p = new Plugin(pluginCfg.id,
            pluginCfg.label,
            pluginCfg.description,
            pluginCfg.componentName,
            pluginCfg.parameters.map((x:IPluginParameter) => new PluginParameter(x.id, x.label, ParameterType[x.type as keyof typeof ParameterType], x.defaultValue)),
            new PluginConfig(pluginCfg.configuration));
          this.plugins.push(p);
        });
        // EventBus.$emit(EVENTS.PLUGIN_CONFIG_LOADED, this.plugins);
      });
    }

    loadCustomPlugins():Promise<AxiosResponse<any>> {
      const url = './config/custom_plugins.json';
      return axios.get(url);
    }

    isActive(pluginName:string) :boolean {
      const p = this.plugins.find((x) => x.componentName === pluginName);
      if (p !== undefined) {
        return p.isActive;
      }
      return false;
    }

    loadPlugins(data: PluginJsonConfig[]) :void{
      if (data && data.length) {
        console.log('load Plugins');
        data.forEach((d) => {
          const plugin = this.plugins.find((w) => w.id === d.plugin);
          if (plugin) {
            plugin.fromJson(d);
            plugin.init();
          }
        });
      }
    }
}
const pluginServiceInstance = new PluginService();
export default PluginService;
export { PluginService, pluginServiceInstance };
