import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    redirect: '/maps',
    component: Home,
  },
  {
    path: '/map/new',
    name: 'Map',
    component: Home,
  },
  {
    path: '/map/:id',
    name: 'MapFromId',
    component: Home,
  },
  {
    path: '/map/wmc/:wmc',
    name: 'MapFromWmc',
    component: Home,
  },
  {
    path: '/maps',
    name: 'MapList',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/MapList.vue'),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
