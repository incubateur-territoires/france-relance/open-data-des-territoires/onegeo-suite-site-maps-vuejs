/* eslint-disable import/no-cycle */
/* eslint-disable no-param-reassign */
/* eslint-disable prefer-destructuring */
/* eslint-disable @typescript-eslint/no-this-alias */
/* eslint-disable class-methods-use-this */
/* eslint-disable block-scoped-var */
/* eslint-disable no-var */
/* eslint-disable no-eval */
/* eslint-disable no-restricted-syntax */
/* eslint-disable camelcase */
import BaseLayer from 'ol/layer/Base';
import GeoJSONFormat from 'ol/format/GeoJSON';
import VectorLayer from 'ol/layer/Vector';
import { Feature } from 'ol';
import sanitizeHtml from 'sanitize-html';
import GeometryType from './GeometryType';
import AbstractLayerTreeElement from './AbstractLayerTreeElement';
import LayerStyle from './LayerStyle';
import AuthenticationInfos from './AuthenticationInfos';
import DatavizConfig from './DatavizConfig';
import LayerStyleComplex from './LayerStyleComplex';
import LayerAttribute from './LayerAttribute';

export default class LayerConfig extends AbstractLayerTreeElement {
  layername!: string;

  title!: string;

  description = '';

  outputFormat!: string;

  protocol!:string;

  mbStyleUrl!:string;

  getFeatureInfoFormat = 'application/json';

  Attribution: any = {};

  metadataUrl = '';

  geometryType!: string;

  filter!: string;

  tableFilter!: string;

  geometricFilter!: string;

  tableGeometricFilter!: string;

  dataviz= new DatavizConfig();

  // Infobulle
  popupInfos = { content: '', active: false, edited: false };

  featureInfoContentRenderer!:(feature:Feature<any>, target:HTMLElement) => void;

  authenticationInfos= new AuthenticationInfos();

  jsonData!: string;

  type!: string;

  projection!: string;

  dataProjection!: string; // projection des données

  styles: any;

  useSldBody = false;

  opacity?: number = 1;

  layerProperties: LayerAttribute[]=Array<LayerAttribute>();

  visible = false;

  editable = false;

  editing = false;

  queryable = true;

  targetNamespace!: string;

  targetPrefix!: string;

  isLegendDisplayed = true;

  legendImageUrl = '';

  public baseLayer = false;

  layerIndex!: number;

  maxScaleDenominator!: number;

  minScaleDenominator!: number;

  boundingBoxEPSG4326!: number[];

  url!: string;

  ol_Layer?: BaseLayer;

  cesium_Layer?:any;

  style = new LayerStyle();

  complexStyle = new LayerStyleComplex();

  maxFeatures = 5000;

  selectedStyle: any;

  error = '';

  idProperty='';

  useProxy = false;

  ignoreUrlInCapabiltiesResponse = false;

  isWFsAble = false; // indique si une couche WMS est aussi disponible en WFS

  constructor(input?: any) {
    super(input);
    if (input) {
      Object.assign(this, input);
      this.style = new LayerStyle(input.style);
      if (!this.Attribution) {
        this.Attribution = {};
      }
      if (input.dataviz) {
        this.dataviz = new DatavizConfig(input.dataviz);
      }
      if (input.layerProperties) {
        this.layerProperties = input.layerProperties.map((x:any) => new LayerAttribute(x));
      }
      if (input.complexStyle && input.complexStyle.styleRules) {
        this.complexStyle = new LayerStyleComplex(this.complexStyle, this.style);
      } else {
        this.complexStyle = new LayerStyleComplex();
      }
      if (input.authenticationInfos) {
        this.authenticationInfos = new AuthenticationInfos(input.authenticationInfos);
        this.loadAuthenticationInfosFromLocalStorage();
      }
      // opacity était entre 0 et 100 et maintenant entre 0 et 1,
      // ceci pour traiter les anciennes cartes
      if (this.opacity && this.opacity > 1) {
        this.opacity /= 100;
      }
    }
  }

  checkSourceAuthentification():void {
    this.loadAuthenticationInfosFromLocalStorage();

    if (this.authenticationInfos !== undefined && this.authenticationInfos.isRequired()) {
      if (this.authenticationInfos.isFilled()) {
        console.log('ok auth infos provided');
      } else {
        this.error = "Cette couche nécessite des informations d'authentication";
      }
    }
  }

  loadAuthenticationInfosFromLocalStorage():void {
    const localInfos = localStorage.getItem('localStorageAuthDB');

    if (localInfos !== undefined && localInfos !== null) {
      const localStorageAuthDB:any = JSON.parse(localInfos);
      const authInfos = localStorageAuthDB[this.url];
      if (authInfos) {
        this.authenticationInfos = new AuthenticationInfos(authInfos);
      }
    }
  }

  hasAuthenticationInfos():boolean {
    return this.authenticationInfos !== undefined && this.authenticationInfos.isFilled();
  }

  isGroup(): boolean {
    return false;
  }

  getGeomAttributeName(): string {
    if (!this.layerProperties) {
      return 'the_geom';
    }
    const geomAttr = this.layerProperties.find((x: { type: string | string[]; }) => x.type && x.type.indexOf('gml:') >= 0);
    if (!geomAttr) {
      return 'the_geom';
    }
    return geomAttr.name;
  }

  getIdPropery(): string {
    const retour = '';
    if (this.idProperty !== '') {
      return this.idProperty;
    }
    if (this.layerProperties && this.layerProperties.length > 0) {
      console.log(this.layerProperties);
      const props = this.layerProperties.filter((x: { name: string; }) => x.name.toLowerCase().indexOf('id') >= 0);
      console.log(props);
      if (props.length > 0) return props[0].name;
    }
    return retour;
  }

  setVisible(visible: boolean): void {
    this.visible = visible;
    if (this.ol_Layer) {
      this.ol_Layer.setVisible(this.isLayerVisible());
    }
    if (this.cesium_Layer) {
      this.cesium_Layer.show = this.isLayerVisible();
    }
  }

  getOpacity() {
    if (this.ol_Layer) {
      return this.ol_Layer.getOpacity();
    }
    if (this.cesium_Layer) {
      return this.cesium_Layer.alpha;
    }
    return undefined;
  }

  setOpacity(opacity: number): void {
    if (this.ol_Layer) {
      this.ol_Layer.setOpacity(opacity);
    }
    if (this.cesium_Layer) {
      this.cesium_Layer.alpha = opacity;
    }
  }

  guessGeometryType(): string {
    let retour = GeometryType.UNDEFINED;
    if (this.ol_Layer && this.ol_Layer instanceof VectorLayer) {
      const layer: VectorLayer<any> = <VectorLayer<any>> this.ol_Layer;
      const features = layer.getSource().getFeatures();
      if (features && features.length > 0) {
        let type = '';
        const geom = features[0].getGeometry();
        if (geom) {
          type = geom.getType();
        }
        if (type === 'MultiLineString' || type === 'LineString') {
          retour = GeometryType.LINE;
        } else if (type === 'MultiPolygon' || type === 'Polygon') {
          retour = GeometryType.POLYGON;
        } else if (type === 'MultiPoint' || type === 'Point') {
          retour = GeometryType.POINT;
        }
      }
    }
    return retour;
  }

  getFilterValue(): string {
    const filterChildByAttribute = new RegExp(/\{([^\}]*)\}/g);
    let filter = '';
    if (this.filter && this.filter.indexOf('{') >= 0) {
      filter = this.filter.replace(filterChildByAttribute, (a, b, c) => eval(b));
    } else if (this.filter) {
      filter = this.filter;
    }
    if (this.geometricFilter && this.geometricFilter.length > 0) {
      if (filter.length > 0) {
        filter = `(${filter} AND ${this.geometricFilter})`;
      } else {
        filter = this.geometricFilter;
      }
    }

    return filter;
  }

  /**
  * Filtre pour la table attributaire
  */
  getTableFilterValue(): string {
    let filter = this.getFilterValue();
    if (filter && filter.length > 0) {
      filter = `(${filter} AND ${this.tableFilter})`;
    } else {
      filter = this.tableFilter;
    }
    if (this.tableGeometricFilter && this.tableGeometricFilter.length > 0) {
      if (filter && filter.length > 0) {
        filter = `(${filter} AND ${this.tableGeometricFilter})`;
      } else {
        filter = this.tableGeometricFilter;
      }
    }
    return filter;
  }

  hasEditionCapabilities():boolean {
    return this.type === 'WFS' || this.type === 'Vector';
  }

  hasRules():boolean {
    return this.complexStyle && this.complexStyle.styleRules.length > 0;
  }

  getStyleFunction(): any {
    let retour:any = (feature:any, resolution:any) => this.style.getStyle(feature, resolution);

    if (this.complexStyle
      && this.complexStyle.styleRules
      && this.complexStyle.styleRules.length > 0) {
      retour = (feature:any, resolution:any) => {
        const s = this.complexStyle.getStyle(feature, resolution);
        if (s) {
          return s;
        }

        return null;
      };
    }
    return retour;
  }

  getCapabilitiesUrl(): string {
    var searchMask = 'getCapabilities';
    var regEx = new RegExp(searchMask, 'ig');
    if (regEx.test(this.url)) {
      return this.url;
    }
    return `${this.url}&request=getCapabilities`;
  }

  getWMTSCapabilitiesUrl(): string {
    var searchMask = 'getCapabilities';
    var regEx = new RegExp(searchMask, 'ig');
    let retour = this.url;
    if (!regEx.test(retour)) {
      retour = `${this.url}&request=getCapabilities`;
    }
    regEx = new RegExp('service(=|%3D)wmts', 'ig');
    if (!regEx.test(retour)) {
      retour = `${this.url}&service=wmts`;
    }
    return retour;
  }

  getFeatureInfoUrl():string {
    if (this.isWMS() && this.url.indexOf('?') >= 0) {
      const i = this.url.indexOf('?');
      const url = `${this.url.slice(0, i + 1)}service=wms&version=1.3.0&request=GetFeatureInfo&QUERY_LAYERS=${this.layername}&layers=${this.layername}&EXCEPTIONS=XML&INFO_FORMAT=application/json&BUFFER=5&FEATURE_COUNT=1&FI_LINE_TOLERANCE=10&FI_POINT_TOLERANCE=25&FI_POLYGON_TOLERANCE=5`;
      return url;
    }
    return '';
  }

  getDownloadUrl(format: any): string|undefined {
    let capabilitiesUrl = this.getCapabilitiesUrl();
    let regEx = new RegExp('getCapabilities', 'ig');
    if (format.type === 'WMS') {
      const urlservice = capabilitiesUrl.replace(regEx, 'GetMap');
      return `${urlservice}&version=1.3.0&format=${format.value}&layers=${this.layername}&WIDTH=1320&HEIGHT=700&CRS=EPSG:4326&BBOX=${this.boundingBoxEPSG4326[1]},${this.boundingBoxEPSG4326[0]},${this.boundingBoxEPSG4326[3]},${this.boundingBoxEPSG4326[2]}`;
    }
    if (this.isWMS()) { // si c'est une couche WMS, on recupère l'url du flux WFS associé
      capabilitiesUrl = this.wmsUrlToWfs(capabilitiesUrl);
    }
    const urlservice = capabilitiesUrl.replace(regEx, 'GetFeature');
    regEx = new RegExp('service(=|%3D)wms', 'ig');
    return `${urlservice.replace(regEx, 'service=WFS')}&outputFormat=${format.value}&TypeNames=${this.layername}&TypeName=${this.layername}`;
  }

  getDescribeFeatureUrl(): string {
    if (this.isWMS() && this.url.indexOf('?') >= 0) {
      const i = this.url.indexOf('?');
      const url = `${this.url.slice(0, i + 1)}service=wfs&request=DescribeFeatureType&TypeNames=${this.layername}&TypeName=${this.layername}&outputFormat=application/json`;
      return url;
    }
    if (this.isWMS() && this.url.indexOf('ows') >= 0) {
      const url = `${this.url}?service=wfs&request=DescribeFeatureType&TypeNames=${this.layername}&TypeName=${this.layername}&outputFormat=application/json`;
      return url;
    }
    if (this.isWFS()) {
      const regEx = new RegExp('request(=|%3D)getCapabilities', 'ig');
      const replaceMask = 'request=DescribeFeatureType';
      const url = `${this.url.replace(regEx, replaceMask)}&TypeNames=${this.layername}&TypeName=${this.layername}`;// &outputFormat=application/json
      return url;
    }
    if (this.isWMTS() || this.isVectorTile()) {
      const serviceRegex = new RegExp('gwc/service/wmts', 'ig');
      const serviceRegEx2 = new RegExp('service(=|%3D)wmts', 'ig');
      let url = this.url.replace(serviceRegex, 'wfs').replace(serviceRegEx2, 'service=wfs');
      const regEx = new RegExp('request(=|%3D)getCapabilities', 'ig');
      const replaceMask = 'request=DescribeFeatureType';
      url = `${url.replace(regEx, replaceMask)}&TypeNames=${this.layername}&TypeName=${this.layername}&outputFormat=application/json`;
      return url;
    }
    return '';
  }

  getFeatureWFSUrl():string {
    const searchMask = 'getCapabilities';
    const regEx = new RegExp(searchMask, 'ig');
    const replaceMask = 'GetFeature';
    return this.getCapabilitiesUrl().replace(regEx, replaceMask);
  }

  getWmtsCapabilitiesUrl():string {
    let url = this.getCapabilitiesUrl();
    var searchMask = 'service(=|%3D)wmts';
    var regEx = new RegExp(searchMask, 'ig');
    if (regEx.test(url)) {
      return url;
    }

    url += '&service=WMTS';
    return url;
  }

  wmsUrlToWfs(url:string):string {
    const serviceRegex = new RegExp('service(=|%3D)wms', 'ig');
    const versionRegex = new RegExp('version(=|%3D)1.3.0', 'ig');
    const wmsRegex = new RegExp('(wms|ows)', 'ig');
    return url.replace(serviceRegex, 'service=wfs').replace(versionRegex, 'version=2.0.0').replace(wmsRegex, 'wfs');
  }

  hasMapBoxStyle():boolean {
    return this.protocol === 'TMS'
      && this.type === 'VectorTile'
      && this.mbStyleUrl !== undefined && this.mbStyleUrl.length > 0;
  }

  isStylable():boolean {
    return this.type !== 'WMTS'
      && !this.hasMapBoxStyle();
  }

  getWFSUrl(extent:any, projection:any): string {
    const urlservice = this.getFeatureWFSUrl();
    let outputformat = 'application/json';
    if (this.outputFormat) {
      outputformat = encodeURIComponent(this.outputFormat);
    }

    let proj = 'EPSG:900913';
    if (this.projection === 'EPSG:3857') {
      proj = 'EPSG:3857';
    }
    let url = `${urlservice
    }&maxfeatures=${this.maxFeatures
    }&count=${this.maxFeatures
    }&typenames=${this.layername}`
      + `&typename=${this.layername}&`
      + `outputFormat=${outputformat}&srsname=${proj}`;

    if (this.filter || this.geometricFilter || this.tableFilter || this.tableGeometricFilter) {
      // const cql_filter = `BBOX(${this.getGeomAttributeName()},${extent.join(',')})
      // AND ${this.getFilterValue()}`;
      /*
      const cql_filter = `${this.getFilterValue()}`;
      const filter = new Filter();
      filter.fromCQL(cql_filter);
      const xmlFilter = filter.toSld();
      url += `&Filter=${xmlFilter}`;
      */
      url += `&CQL_FILTER=BBOX(${this.getGeomAttributeName()},${extent.join(',')},'${proj}') AND ${encodeURIComponent(this.getTableFilterValue())}`;
    } else {
      url += `&bbox=${extent.join(',')},${proj}`;
    }
    return url;
  }

  updateStyle() :void{
    if (this.type === 'WMS' && !this.hasMapBoxStyle()) {
      if (this.useSldBody) {
        const sld = this.styletoSld();
        this.getLayerSource().updateParams({ SLD_BODY: sld });
      } else {
        this.getLayerSource().updateParams({ SLD_BODY: undefined });
      }
    } else if (!this.hasMapBoxStyle()) {
      this.style.clearCache();
      /* TODO
      if (layer.dynamicStyle) {
        layer.dynamicStyle.clearStylesCache();
      }
      */
      if (this.getOlLayer() && this.getOlLayer().setStyle) {
        this.getOlLayer().setStyle(this.getStyleFunction());
      }
    }
  }

  styletoSld(): string {
    let rules = `<Rule>${this.style.toSld(this)}</Rule>`;
    if (this.complexStyle.isDefined()) {
      rules = this.complexStyle.toSld(this);
    }

    const sld = `<?xml version="1.0" encoding="ISO-8859-1"?>
        <StyledLayerDescriptor version="1.0.0"
            xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
            xmlns="http://www.opengis.net/sld"
            xmlns:ogc="http://www.opengis.net/ogc"
            xmlns:xlink="http://www.w3.org/1999/xlink"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
          <NamedLayer>
            <Name>${this.layername}</Name>
            <UserStyle>
                <Title>${this.layername} style</Title>
                <Abstract></Abstract>
                <FeatureTypeStyle>
                  ${rules}
                </FeatureTypeStyle>
            </UserStyle>
            </NamedLayer>
        </StyledLayerDescriptor>`;
    return sld;
  }

  toJson():any {
    var newLayer = {
      layername: this.layername,
      title: this.title,
      type: this.type,
      visible: this.isVisible(),
      useSldBody: this.useSldBody,
      dataviz: this.dataviz.toJson(),
      editable: this.editable,
      queryable: this.queryable,
      protocol: this.protocol,
      mbStyleUrl: this.mbStyleUrl,
      idProperty: this.idProperty,
      targetNamespace: this.targetNamespace,
      targetPrefix: this.targetPrefix,
      dataProjection: this.dataProjection,
      layerIndex: this.layerIndex,
      maxScaleDenominator: this.maxScaleDenominator,
      minScaleDenominator: this.minScaleDenominator,
      boundingBoxEPSG4326: this.boundingBoxEPSG4326,
      filter: this.filter,
      geometricFilter: this.geometricFilter,
      outputFormat: this.outputFormat,
      getFeatureInfoFormat: this.getFeatureInfoFormat,
      url: this.url,
      baseLayer: this.baseLayer,
      opacity: this.ol_Layer ? this.ol_Layer.getOpacity() : 1,
      isLegendDisplayed: this.isLegendDisplayed,
      legendImageUrl: this.legendImageUrl,
      description: this.description,
      popupInfos: this.popupInfos,
      style: this.style.toJson(),
      complexStyle: {},
      Attribution: this.Attribution,
      metadataUrl: this.metadataUrl,
      geometryType: this.geometryType,
      ignoreUrlInCapabiltiesResponse: this.ignoreUrlInCapabiltiesResponse,
      layerProperties: Array<any>(),
      authenticationInfos: {},
      jsonData: '',
    };

    if (this.complexStyle) {
      newLayer.complexStyle = this.complexStyle.toJson();
    }
    if (this.layerProperties) {
      newLayer.layerProperties = this.layerProperties.map((x:LayerAttribute) => x.toJson());
    }
    // on ne sauve pas les infos d'authentification dans la config de la carte
    // elles sont stockées dans le localstorage si l'utilisateur le demande
    if (this.authenticationInfos && this.authenticationInfos.isRequired()) {
      newLayer.authenticationInfos = { type: this.authenticationInfos.type };
    }
    if (this.type === 'Vector' && this.url === undefined && this.ol_Layer && this.ol_Layer instanceof VectorLayer) {
      const feats = this.getLayerSource().getFeatures();
      const featstring = (new GeoJSONFormat()).writeFeatures(feats);
      const sizeInko = Number(featstring.length / 1024);
      if (sizeInko > 500) {
        this.error = `Trop de données dans cette couche pour la sauvegarde( ${sizeInko} ko), vous devez supprimer des objets`;
        throw new Error('TooMuchDatas');
      } else {
        newLayer.jsonData = featstring;
      }
    }

    if (this.baseLayer) {
      newLayer.layerIndex = 0;
    }

    return newLayer;
  }

  getLegendImageUrl(): string {
    if (this.legendImageUrl) {
      return this.legendImageUrl;
    }
    let url;
    if (this.ol_Layer && this.getLayerSource().urls) {
      url = this.getLayerSource().urls[0];
    } else if (this.ol_Layer) {
      url = this.getLayerSource().getUrl();
    }
    url += '&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&WIDTH=30&HEIGHT=30&LAYER=';

    url += this.layername;

    if (this.useSldBody) {
      const sld = this.styletoSld();
      url += `&SLD_BODY=${encodeURIComponent(sld)}`;
    }

    return url;
  }

  getAttribution(): string | undefined {
    let attributions;
    if (this.Attribution && this.Attribution.OnlineResource) {
      const url = encodeURIComponent(this.Attribution.OnlineResource);
      attributions = `<a href="${url}" target="_blank">${sanitizeHtml(this.Attribution.Title)}</a> `;
    } else if (this.Attribution) {
      attributions = sanitizeHtml(this.Attribution.Title);
    }
    return attributions;
  }

  setOL_Layer(layer: BaseLayer): void {
    this.ol_Layer = layer;
    (<any>layer).customConfig = this;
  }

  setCesium_Layer(layer: any): void {
    this.cesium_Layer = layer;
    (<any>layer).customConfig = this;
  }

  getCesiumLayer(): any {
    return this.cesium_Layer;
  }

  getOlLayer(): any {
    return this.ol_Layer;
  }

  getLayerSource():any {
    return this.getOlLayer().getSource();
  }

  isWMS():boolean {
    return this.type === 'WMS';
  }

  hasTableCapacity():boolean {
    return this.hasWFSCapacity() || this.isVector() || this.isVectorTile();
  }

  isVector():boolean {
    return this.type === 'Vector';
  }

  isVectorTile():boolean {
    return this.type === 'VectorTile';
  }

  isWFS():boolean {
    return this.type === 'WFS';
  }

  hasWFSCapacity():boolean {
    return this.type === 'WFS' || this.isWFsAble;
  }

  isWMTS():boolean {
    return this.type === 'WMTS';
  }
}
