export default class Application {
  id!: number;

  // eslint-disable-next-line camelcase
  display_name!: string;

  description!: string;

  // idUserOwner!: number; deprecated

  config!: string;

  thumbnail = null;

  // eslint-disable-next-line camelcase
  public_access = false;

  usergroup!: number;

  constructor(input?: unknown) {
    console.log(input);
    if (input) {
      Object.assign(this, input);
    }
  }

  setTumbnail(blob:any) :void{
    this.thumbnail = blob;
  }
}
