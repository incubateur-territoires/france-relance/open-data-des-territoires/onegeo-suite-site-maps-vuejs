export default class GeometryType {
  static UNDEFINED = 'indéfini';

  static POINT = 'point';

  static LINE = 'ligne';

  static POLYGON = 'polygone';
}
