import {
  Icon, Fill, Stroke, Style, RegularShape, Circle as CircleStyle, Text,
} from 'ol/style';
import Feature from 'ol/Feature';
import MapUtils from '../MapUtils';
import { TextPlacement } from 'ol/style/Text';

export default class TextStyle {
    private baseStyle!: TextStyle;

    text = '';

    padding!: number;

    offsetX: number|undefined = undefined;

    offsetY: number|undefined = undefined;

    textFontSize!: number;

    textFillColor!: string;

    textAlignement!: CanvasTextAlign;

    textStrokeColor!: string;

    textBackGroundFillColor!: string;

    textBackGroundStrokeColor!: string;

    textRotation!: number;

    constructor(json: any = null) {
      if (json) {
        Object.assign(this, json);
      }
    }

    public setBaseStyle(baseStyle: TextStyle):void {
      this.baseStyle = baseStyle;
    }

    toJson():any {
      return {
        text: this.text,
        padding: this.padding,
        offsetX: this.offsetX,
        offsetY: this.offsetY,
        textFontSize: this.textFontSize,
        textFillColor: this.textFillColor,
        textAlignement: this.textAlignement,
        textStrokeColor: this.textStrokeColor,
        textBackGroundFillColor: this.textBackGroundFillColor,
        textBackGroundStrokeColor: this.textBackGroundStrokeColor,
        textRotation: this.textRotation,
      };
    }

    setDefaultValues():void {
      this.text = '';
      this.padding = 0;
      this.textFillColor = '#000000';
      this.textStrokeColor = 'rgba(200, 200, 200, 1)';
      this.textBackGroundFillColor = '';
      this.textBackGroundStrokeColor = '';
      this.textFontSize = 11;
      this.offsetX = 0;
      this.offsetY = 0;
      this.textAlignement = 'center';
      this.textRotation = 0;
    }

    public createTextStyle(feature?: Feature<any>):Text {
      const align:CanvasTextAlign = this.getTextAlignement();// ou end,left,right,start
      const baseline:CanvasTextBaseline = 'middle';
      const size = this.getTextFontSize();
      const offsetX = this.getOffsetX();
      const offsetY = this.getOffsetY() * -1;
      const weight = 'normal';// ou bold
      const placement:TextPlacement = 'point';// ou line
      const maxAngle = 45;
      const overflow = false;
      const rotation = (this.getTextRotation() * Math.PI) / 180;

      const font = `${weight} ${size}px "Open Sans", "Arial Unicode MS", "sans-serif"`;
      const fillColor = this.getTextFillColor();
      const backGroundfillColor = this.getTextBackGroundFillColor();
      const backGroundstrokeColor = this.getTextBackGroundStrokeColor();
      const outlineColor = this.getTextStrokeColor();

      const outlineWidth = 1;
      const options = {
        textAlign: align,
        textBaseline: baseline,
        font,
        padding: this.getTextPadding(),
        text: this.getTextContent(feature),
        fill: new Fill({ color: fillColor }),
        stroke: new Stroke({ color: outlineColor, width: outlineWidth }),
        offsetX,
        offsetY,
        placement,
        maxAngle,
        overflow,
        rotation,
      };
      if (backGroundfillColor && backGroundfillColor.length > 0) {
        (<any>options).backgroundFill = new Fill({ color: backGroundfillColor });
      }
      if (backGroundstrokeColor && backGroundstrokeColor.length > 0) {
        (<any>options).backgroundStroke = new Stroke({ color: backGroundstrokeColor });
      }
      return new Text(options);
    }

    private getTextContent(feature?: Feature<any>) {
      return this.getText().replace(/{(.*?)}/, (a, b) => {
        const value = feature ? feature.get(b) : null;

        return value || '';
      });
    }

    getOffsetY():any {
      return this.offsetY !== undefined ? this.offsetY : this.baseStyle.getOffsetY();
    }

    getOffsetX():any {
      return this.offsetX !== undefined ? this.offsetX : this.baseStyle.getOffsetX();
    }

    getTextPadding():any {
      return this.padding !== undefined
        ? [this.padding, this.padding, this.padding, this.padding]
        : this.baseStyle.getTextPadding();
    }

    getTextBackGroundFillColor(): string {
      return this.textBackGroundFillColor !== undefined
        ? this.textBackGroundFillColor : this.baseStyle.getTextBackGroundFillColor();
    }

    getTextBackGroundStrokeColor(): string {
      return this.textBackGroundStrokeColor !== undefined
        ? this.textBackGroundStrokeColor : this.baseStyle.getTextBackGroundStrokeColor();
    }

    getTextFontSize(): number {
      return this.textFontSize ? this.textFontSize : this.baseStyle.getTextFontSize();
    }

    getFontSld(): string {
      return `<Font>
        <CssParameter name="font-family">Arial</CssParameter>
        <CssParameter name="font-size">${this.getTextFontSize()}</CssParameter>
        <CssParameter name="font-style">normal</CssParameter>
        <CssParameter name="font-weight">bold</CssParameter>
      </Font>`;
    }

    getAnchorPointSld(): string {
      let anchor = `<AnchorPoint>
                        <AnchorPointX>0.5</AnchorPointX>
                        <AnchorPointY>0.5</AnchorPointY>
                    </AnchorPoint>`;

      if (this.getTextAlignement() === 'start' || this.getTextAlignement() === 'left') {
        anchor = `<AnchorPoint>
                    <AnchorPointX>0.0</AnchorPointX>
                    <AnchorPointY>0.5</AnchorPointY>
                 </AnchorPoint>`;
      }
      if (this.getTextAlignement() === 'end' || this.getTextAlignement() === 'right') {
        anchor = `<AnchorPoint>
                    <AnchorPointX>1.0</AnchorPointX>
                    <AnchorPointY>0.5</AnchorPointY>
                  </AnchorPoint>`;
      }
      return anchor;
    }

    textSymbolizerToSld():string {
      let txtSld = '';
      if (this.getText()) {
        let label = '';
        if (this.getText().indexOf('{') >= 0) {
          label = `<Label>
                <ogc:PropertyName>${this.getText().replace(/\{|\}/g, '')}</ogc:PropertyName>
              </Label>`;
        } else {
          label += `<Label>
                <ogc:Literal>${this.getText()}</ogc:Literal>
              </Label>`;
        }

        txtSld = `<TextSymbolizer>
            ${label}
            ${this.getFontSld()}
            <LabelPlacement>
              <PointPlacement>
               ${this.getAnchorPointSld()}
                <Displacement>
                  <DisplacementX>${this.getOffsetX()}</DisplacementX>
                  <DisplacementY>${this.getOffsetY()}</DisplacementY>
                </Displacement>
                <Rotation>${this.getTextRotation()}</Rotation>
              </PointPlacement>
            </LabelPlacement>
            <Fill>
              <CssParameter name="fill">${MapUtils.rgba2hex(this.getTextFillColor())}</CssParameter>
            </Fill>
          </TextSymbolizer>`;
      }

      return txtSld;
    }

    getText(): string {
      return this.text !== undefined ? this.text : this.baseStyle.getText();
    }

    getTextAlignement(): CanvasTextAlign {
      return this.textAlignement !== undefined
        ? this.textAlignement : this.baseStyle.getTextAlignement();
    }

    getTextFillColor(): string {
      return this.textFillColor ? this.textFillColor : this.baseStyle.getTextFillColor();
    }

    getTextRotation(): number {
      return this.textRotation !== undefined
        ? this.textRotation : this.baseStyle.getTextRotation();
    }

    getTextStrokeColor(): string {
      return this.textStrokeColor ? this.textStrokeColor : this.baseStyle.getTextStrokeColor();
    }
}
