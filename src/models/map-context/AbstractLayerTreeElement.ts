/* eslint-disable class-methods-use-this */
/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line import/no-cycle
import LayerGroupConfig from './LayerGroupConfig';

export default class AbstractLayerTreeElement {
  visible = false;

  id!: string;

  public static counter = 0;

  parent?: LayerGroupConfig;

  constructor(input?: any) {
    if (input) {
      Object.assign(this, input);
    }
    if (!this.id) {
      AbstractLayerTreeElement.counter += 1;
      this.id = `generated_${AbstractLayerTreeElement.counter}`;
    }
  }

  isLayerVisible(): boolean {
    if (this.parent) {
      return this.parent.isLayerVisible() && this.visible;
    }
    return this.visible;
  }

  isVisible(): boolean {
    return this.visible;
  }

  toJson(): any {
    return {};
  }
}
