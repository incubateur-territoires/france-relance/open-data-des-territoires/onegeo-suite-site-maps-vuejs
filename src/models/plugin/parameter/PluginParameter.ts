import { ParameterType } from '../ParameterType';

export default class PluginParameter<T> {
    id: string;

    label: string;

    description?: string;

    type: ParameterType;

    value: T;

    constructor(id: string, label: string, type: ParameterType,
      value: T, description?:string) {
      this.id = id;
      this.label = label;
      this.type = type;
      this.value = value;
      this.description = description;
    }

    setValue(val:T) :void{
      this.value = val;
    }
}
