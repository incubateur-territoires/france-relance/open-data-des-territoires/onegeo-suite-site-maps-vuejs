import { SelectItem } from './SelectItem';
import { ParameterType } from '../ParameterType';
import PluginParameter from './PluginParameter';

export default class PluginSelectParameter<T> extends PluginParameter<T> {
  choices:SelectItem[];

  constructor(id: string, label: string, value: T,
    choices:SelectItem[],
    description?:string) {
    super(id, label, ParameterType.Select, value, description);
    this.choices = choices;
  }
}
