export interface IPluginParameter
{
    id: string;

    label: string;

    type :string;

    defaultValue: any;
}
