export interface PluginJsonConfig
{
    plugin: string;

    configuration: {isActive: boolean};

    parameters: {id: string, value: any}[];
}
