import { PluginLocation } from './PluginLocation';

export default class PluginConfig {
    active = false;

    position = PluginLocation.TopLeft;

    icon = null;

    constructor(json: any = null) {
      this.fromJson(json);
    }

    fromJson(json: any) :void{
      if (json) {
        if (json.icon) {
          this.icon = json.icon;
        }

        this.active = json.isActive === true

        || (json.isActive && json.isActive === true);
        console.log(json);
        if (json.position !== undefined && !Number.isNaN(Number(json.position))) {
          this.position = Number(json.position);
        } else if (json.position !== undefined) {
          this.position = PluginLocation[json.position as keyof typeof PluginLocation];
        }
      }
    }

    clone(): PluginConfig {
      return new PluginConfig({
        isActive: this.active,
        position: this.position,
        icon: this.icon,
      });
    }
}
