/* eslint-disable camelcase */
export interface RemoteServer
{
  nom: string,
  type: string,
  url: string,
  ignoreUrlInCapabiltiesResponse:boolean,
}

export interface OGCLayer
{
  Title:string,
  Abstract?:string,
  OrganisationName?:string,
  ThumbnailUrl?:string,
  Url?:string,
  Type?:string,
  Name:string,
  Style?:any,
  Attribution?:any,
  MinScaleDenominator?:number,
  MaxScaleDenominator?:number,
  WGS84BoundingBox:any,
  CRS:any,
  Format?:any,
  EX_GeographicBoundingBox?:any,
  Identifier:string,
  outputFormat?:string,
  status?:string,
  MetadataURL?:any[],
  Layer?:OGCLayer[] // subLayers pour les groupes de couches
}

export interface CapabilitiesResponse
{
  type:string,
  useProxy:boolean,
  layers:OGCLayer[],
}
