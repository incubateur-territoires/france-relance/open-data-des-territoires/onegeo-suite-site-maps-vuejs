import '@babel/polyfill';
import 'mutationobserver-shim';
import vSelect from 'vue-select';
import Vue from 'vue';
import './plugins/bootstrap-vue';
import CKEditor from '@ckeditor/ckeditor5-vue2';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import 'ol/ol.css';
import './styles/app.less';
import 'vue-select/dist/vue-select.css';

Vue.component('v-select', vSelect);

Vue.use(CKEditor);
Vue.config.productionTip = false;
Vue.config.devtools = true;
console.log('init error Handler');
Vue.config.errorHandler = (err, vm, info) => {
  // err: error trace
  // vm: component in which error occured
  // info: Vue specific error information such as lifecycle hooks, events etc.
  // TODO: Perform any custom logic or log to server test
  console.log(err);
};
window.onerror = function (message, source, lineno, colno, error) {
  console.log(message);
};
new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
