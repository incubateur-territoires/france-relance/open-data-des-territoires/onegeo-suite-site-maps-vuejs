/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import Plugin from '@/models/plugin/Plugin';
import Component from 'vue-class-component';
import BaseComponent from '../BaseComponent';

@Component({
  props: {
    plugin: {},
  },
  components: {
  },
})
export default class PluginComponent extends BaseComponent {
  plugin!:Plugin;
}
