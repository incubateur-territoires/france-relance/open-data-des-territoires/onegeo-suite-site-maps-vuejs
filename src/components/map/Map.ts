/* eslint-disable camelcase */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import Component from 'vue-class-component';
import MapPluginContainer from '@/components/map/MapPluginContainer.vue';
import EditionToolBar from '@/components/map/EditionToolBar.vue';
import LeftSideBar from '@/components/LeftSideBar.vue';
import FeatureInfosPanel from '@/components/map/FeatureInfosPanel.vue';
import { EventBus, EVENTS } from '@/services/EventBus';
import LayerConfig from '@/models/map-context/LayerConfig';
import BaseComponent from '../BaseComponent';

@Component({
  props: {
    msg: String,
  },
  components: {
    MapPluginContainer,
    EditionToolBar,
    LeftSideBar,
    FeatureInfosPanel,
  },
})
export default class Map extends BaseComponent {
  msg!: string;

  editedLayer:LayerConfig|null=null;

  cesiumMap = true;

  created() {
    // this.mapService = mapservice;
  }

  mounted() {
    const cesium = this.$route.query['3d'];
    if (cesium !== undefined && cesium === 'true') {
      this.cesiumMap = true;
    } else {
      this.cesiumMap = false;
    }

    console.log('loading map component');
    EventBus.$on(EVENTS.MAP_INIT_END, () => this.afterMapInit());
    EventBus.$on(EVENTS.MAP_CURRENT_BBOX_CHANGE, (bbox:any) => this.onMoveEnd(bbox));
    this.pluginService.loadPlugins(this.contextService.getCurrentContext().plugins);
    this.getMapService().initMap(this.contextService.getCurrentContext());
    EventBus.$on(EVENTS.START_EDITION, (layer:LayerConfig) => this.onEditStart(layer));
    EventBus.$on(EVENTS.STOP_EDITION, (layer:LayerConfig) => this.onEditStop(layer));
  }

  onMoveEnd(bbox:any) {
    // this.$route.query.bbox=bbox[0]+","+bbox[1]+","+bbox[2]+","+bbox[3];
    const url = new URL(window.location.href);
    console.log(url);
    const hash = window.location.hash.substr(1);
    const hashSplitted = window.location.hash.substr(1).split('?');
    let hashParams:any = {};
    if (hashSplitted.length > 1) {
      hashParams = hashSplitted[1].split('&').reduce((res:any, item) => {
        const parts = item.split('=');
        res[parts[0]] = parts[1];
        return res;
      }, {});
    }

    hashParams.bbox = `${bbox[0]},${bbox[1]},${bbox[2]},${bbox[3]}`;
    console.log(hashParams);
    const stringHashParams = Object.keys(hashParams).map((key) => [key, hashParams[key]].join('=')).join('&');
    url.hash = `#${hashSplitted[0]}?${stringHashParams}`;

    window.history.replaceState('', '', url.href);
  }

  afterMapInit() {
    const { bbox, camera_pos } = this.$route.query;
    if (this.is3DView()
      && camera_pos !== undefined
      && camera_pos !== null
      && !Array.isArray(camera_pos)) {
      const camera_posArray:number[] = camera_pos.split(',').map((x) => parseFloat(x));
      const heading:any = this.$route.query.camera_heading;
      const pitch:any = this.$route.query.camera_pitch;
      const roll:any = this.$route.query.camera_roll;
      this.getCesiumMapService().setCameraView(camera_posArray,
        parseFloat(heading), parseFloat(pitch), parseFloat(roll));
    } else if (bbox !== undefined && bbox !== null && !Array.isArray(bbox)) {
      const bboxWGS84 = bbox.split(',');
      const numberbboxWGS84:number[] = bboxWGS84.map((x) => parseFloat(x));
      this.getMapService().zoomToBboxWGS84(numberbboxWGS84);
    }
    const { addLayer } = this.$route.query;
    if (addLayer !== undefined && !Array.isArray(addLayer) && addLayer.length > 0) {
      const layerConfig = JSON.parse(atob(addLayer));
      const bboxWGS = layerConfig.bbox_by_projection['EPSG:4326'];
      const newLayer = new LayerConfig({
        layername: layerConfig.name,
        url: layerConfig.url,
        title: layerConfig.description,
        description: layerConfig.description,
        type: 'WMS',
        outputFormat: layerConfig.formats[0],
        visible: true,
        // projection: layerConfig.projections[0],
        boundingBoxEPSG4326: [bboxWGS.minx, bboxWGS.miny, bboxWGS.maxx, bboxWGS.maxy],
      });
      console.log(layerConfig);
      this.addLayerToMap(newLayer);
      this.getMapService().zoomToLayer(newLayer);
    }
  }

  private addLayerToMap(newLayer: LayerConfig) {
    try {
      this.contextService.getCurrentContext().getLayers().unshift(newLayer);
      this.getMapService().addLayer(newLayer);
    } catch (error) {
      console.error(error);
    }
  }

  beforeDestroy() {
    this.getMapService().destroyMap();
    this.contextService.currentContext = null;
    this.contextService.currentApplication = null;
  }

  onEditStart(layer:LayerConfig) :void{
    this.editedLayer = layer;
  }

  onEditStop(layer:LayerConfig) {
    this.editedLayer = null;
  }
}
