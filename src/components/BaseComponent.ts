/* eslint-disable prefer-destructuring */
/* eslint-disable dot-notation */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import Vue from 'vue';
import Component from 'vue-class-component';
import { MapService, mapServiceInstance } from '@/services/MapService';
import { CesiumMapService, cesiumMapServiceInstance } from '@/services/CesiumMapService';
import { contexteServiceInstance, ContextService } from '@/services/ContextService';
import { pluginServiceInstance, PluginService } from '@/services/PluginService';
import { CapabilitiesService, capabilitiesServiceInstance } from '@/services/CapabilitiesService';

@Component
export default class BaseComponent extends Vue {
  private mapService: MapService = mapServiceInstance;

  public cesiumMapService: CesiumMapService = cesiumMapServiceInstance;

  public contextService: ContextService = contexteServiceInstance;

  public pluginService: PluginService = pluginServiceInstance;

  public capabilitiesService: CapabilitiesService = capabilitiesServiceInstance;

  getCesiumMapService() {
    if (this.is3DView()) {
      return this.cesiumMapService;
    }
    throw new Error('3D view is not loaded');
  }

  getOpenLayersMapService() {
    if (this.is3DView()) {
      throw new Error('3D view is not loaded');
    }
    return this.mapService;
  }

  getMapService() {
    if (this.is3DView()) {
      return this.cesiumMapService;
    }
    return this.mapService;
  }

  is3DView() :boolean {
    const cesium = this.$route.query['3d'];
    if (cesium !== undefined && cesium === 'true') {
      return true;
    }
    return false;
  }

  public get debug():boolean {
    const debug = this.$route.query['debug'];
    if (debug !== undefined && debug === 'true') {
      return true;
    }
    return false;
  }

  public get beta():boolean {
    const beta = this.$route.query['beta'];
    if (beta !== undefined && beta === 'true') {
      return true;
    }
    return false;
  }
}
