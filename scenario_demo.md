

URL : https://preprod.pigma.org/onegeo-maps

WebSIG: 
plusieurs usages:
* WebSIG comme QGIS pour consulter une donnée, croiser plusieur données mais sans enregistrer son travail.
* utilisé comme outils de valorisation de données pour les mettres en contexte avec d'autres données
* utilisé pour créer des applications metiers, pour éditer des données de manière collaborative par exemple


une application :
 - ensemble de couches de données organisée, configurés, stylés
 - un ensemble d'outils ou plugins activés et configurés
 - une description de tout ça et une gestion des droits associées

 

1/ Lister les cartes publiques (ne marche pas pour l'instant )
=> https://preprod.pigma.org/onegeo-maps/#/maps/ 

2/ Se connecter et créer une carte

* Bouton "SE CONNECTER" 
* Bouton + "Nouvelle Carte"
* Ouvrir le Panneau de gauche "Réglages" pour changer titre / description / cliquer sur "Faire une copie d'écran" / Se déplacer et cliquer sur "Emprise courante de la carte" / cliquer sur "Application Ouverte à tous" pour la rendre publique
* Enregistrer les modifications => la carte est créé mais elle ne contient pas de données à part le fond OSM


3/ Ajouter une couche WMTS
* Pour repasser en édition => cliquer sur le menu en haut à droite puis cliquer sur "Editer la carte" 
* Ouvrir le Panneau de gauche "Couches de données" 
* Cliquer sur "Rechercher des données"
* Choisir le serveur "magosm WMTS" => choisir la couche "Réseau routier" => cliquez sur "ajouter à la carte"

3/ Ajouter une couche en VectorTile (si correction déployé en préprod)
* Cliquer sur "Rechercher des données"
* Choisir le serveur "magosm WMTS" => choisir la couche "Emprise des départements" => cliquez sur "ajouter en vecteur tuilé"
* Cliquer sur la couche pour faire apparaitre les boutons d'actions => jouer sur la transparence / Zoomer sur la couche
* Cliquer sur le bouton "Configurer la couche" : Définir le type de géométrie => le style peut etre modifié dans l'onglet style

4/ Ajouter une couche en WFS
* Cliquer sur "Rechercher des données"
* Choisir le serveur "magosm WFS" => choisir la couche "Localisations des hopitaux" => cliquez sur "ajouter à la carte"
* Si c'est long => aller dans la configuration et mettre "Nombre d'objets max affichés" à 100
* Ajouter un filter emergency = 'yes' pour n'avoir que les hopitaux avec des urgences
* Dans l'onglet principal, définir type de geometrie à point puis Dans l'onglet style changer le style => type de symbole => SVG => choisir un picto dans "santé"

5/ Passer de WFS à WMS
* Dans l'onglet principal, cliquer sur le bouton vert "Passer en raster" puis valider
* La couche est maintenant une couche WMS mais le filtre est toujours actif
* Retourner dans la configuration de la couche => onglet style => cochez "Utiliser le style défini ici pour les requêtes WMS." puis valider
* la couche WMS utilise maintenant le style défini par l'appli
* Attention pour l instant bug sur le passage de WMS à WFS

6/ Edition
* Ajouter une autre couche WFS, emprise des EPCI par exemple (Nombre d'objets max affichés à 100 si ca rame )
* dans la configuration de la couche, choisir type de géometrie à Polygone et cochez 'Editable'
* un crayon jaune apparait pour basculer en edition => cliquer dessus => jouer avec la toolbar d'édition 
* la sauvegarde n'est pas effective car on a pas les droits en edition sur le service

7/ Activer des modules
* Pour repasser en édition => cliquer sur le menu en haut à droite puis cliquer sur "Editer la carte" 
* Ouvrir le Panneau de gauche "Modules" 
* Activer la recherche de lieux, l'échelle etc... => les panneaux s'activent en fonction
* Modifier les paramètres du module coordonnées (par ex changer le format ou la projection) => validez => l'interface est mise à jour


https://wxs.ign.fr/inspire/inspire/r/wms?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetCapabilities


https://download.data.grandlyon.com/wms/grandlyon?service=wms&request=GetCapabilities


https://download.data.grandlyon.com/wfs/grandlyon?service=wfs&request=GetCapabilities&version=2.0.0


let filt="BBOX(the_geom,-1719612.4464747347,4653963.098746597,1512245.0831072633,6369910.665226788) AND test='test'";
let filter=new Filter();
filter.fromCQL(filt);
console.log(filter.toSld());

https://download.data.grandlyon.com/wfs/grandlyon?service=wfs&request=GetFeature&version=2.0.0&maxfeatures=5000&count=5000&typenames=ms:adr_voie_lieu.adrtoilettepublique_latest&typename=ms:adr_voie_lieu.adrtoilettepublique_latest&outputFormat=application%2Fjson%3B%20subtype%3Dgeojson&srsname=EPSG:3857&Filter=%3CFilter%20xmlns=%22http://www.opengis.net/fes/2.0%22%3E%3CAnd%3E%3CBBOX%3E%3CValueReference%3EmsGeometry%3C/ValueReference%3E%3CEnvelope%20xmlns=%22http://www.opengis.net/gml/3.2%22%3E%3ClowerCorner%3E456575.47010143736%205697926.944977101%3C/lowerCorner%3E%3CupperCorner%3E608576.4689883265%205778475.736858282%3C/upperCorner%3E%3C/Envelope%3E%3C/BBOX%3E%3CPropertyIsEqualTo%3E%3CValueReference%3Ecodinsee%3C/ValueReference%3E%3CLiteral%3E69202%3C/Literal%3E%3C/PropertyIsEqualTo%3E%3C/And%3E%3C/Filter%3E

