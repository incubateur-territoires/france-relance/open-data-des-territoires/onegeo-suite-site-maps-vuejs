# onegeo-maps

[[_TOC_]]
* [Mise en place de l'environnement de développement](docs/documentation_technique/README.md) dans [Documentation Technique](docs/documentation_technique/README.md)

## Plateforme de démonstration

https://demo.onegeo.fr/onegeo-maps/#/maps


## Intégration continue : CI 


| Job    | Branches | Description | Manuel/Auto | 
| ------ | ------   | ------ | ------ |
| sonarqube-check | toutes | Vérfication qualité du code source | Auto  |
| build_development | draft | Build pour la PF de dev pigma | Auto  |
| deploy-pigma-dev | draft | Déploiement sur la PF de dev Pigma | Auto  |

### Liste des fonctionnalités

* Actuelles voir [Documentation Fonctionnelle](docs/documentation_fonctionnelle/doc.md) 
* A venir voir [ROADMAP](https://git.neogeo.fr/neogeo/onegeosuite/deploiement/-/blob/master/roadmap_maps.md)

### condition pour l'édition d'une couche

la table doit contenir une clef primaire
dans l'entrepot, cocher 'expose primary key'

