module.exports = {
  publicPath: './',
  lintOnSave: false,
  pwa: {
    workboxOptions: {
      skipWaiting: true,
    },
  },
  css: {
    loaderOptions: {
      less: {
        globalVars: {
        },
      },
    },
  },
};
